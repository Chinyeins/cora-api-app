<?php

use App\Participants;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Get all competitions
 *
 * @param integer $num
 */
Route::get('/competitions', function (Request $request) {
	$num = !empty($request->get('num')) ? $request->get('num') : null;

	$response = array(
		'data' => \App\Competitions::getAll($num),
		'error' => 0
	);

	if(isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	if(isset($response['data']))
		$response['size'] = count($response['data']);

	return response($response, 200)
		->header('Content-Type', 'application/json');
});


/**
 * Get a specific competition
 *
 * @param integer $num
 */
Route::get('/competition/{id}', function (Request $request, $id) {
	$response = array(
		'data' => \App\Competitions::getByID($id),
		'error' => 0
	);

	if(isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	return response($response, 200)
		->header('Content-Type', 'application/json');
});


/**
 * All Competitions by UserID - my events
 */
Route::get('/user/competitions/{id}', function (Request $request, $id) {
	$response = array(
		'data' => \App\Competitions::getByUserID( $id ),
		'error' => 0
	);

	if(isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	return response($response, 200)
		->header('Content-Type', 'application/json');
});

/**
 * Get User data by id - (Profile)
 */
Route::get('/user/{id}', function (Request $request, $id) {
	$response = array(
		'data' => \App\User::getByID($id),
		'error' => 0
	);

	if(isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	return response($response, 200)
		->header('Content-Type', 'application/json');
});


/**
 * Enlist user to competition
 */
Route::get('/user/enlist/{id}/{compID}', function (Request $request, $userID, $compID) {
	$response = array();
	$data = array();
	$data['error'] = 0;

	if($userID <= 0 || $compID <= 0) {
		//TODO: quick check if userID and COMP ID valid
		$data['error'] = 1;

		return response($response, 200)
			->header('Content-Type', 'application/json');
	}

	$isSubscribbed = Participants::enList($userID, $compID);
	if($isSubscribbed == false) {
		$data['error'] = 1;
	}

	$response['data'] = $data;
	return response($response, 200)
		->header('Content-Type', 'application/json');
});

/**
 * Unlist user to competition
 */
Route::get('/user/unlist/{id}/{compID}', function (Request $request, $userID, $compID) {
	$response = array();
	$data = array();
	$data['error'] = 0;

	if($userID <= 0 || $compID <= 0) {
		//TODO: quick check if userID and COMP ID valid
		$data['error'] = 1;

		return response($response, 200)
			->header('Content-Type', 'application/json');
	}

	$isSubscribbed = Participants::unList($userID, $compID);
	if($isSubscribbed == false) {
		$data['error'] = 1;
	}

	$response['data'] = $data;
	return response($response, 200)
		->header('Content-Type', 'application/json');
});


/***
 * Users All

 */
Route::get('/users', function (Request $request) {
	$response = array(
		'data' => \App\User::all(),
		'error' => 0
	);

	if( isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	return response($response, 200)
		->header('Content-Type', 'application/json');
});


/**
 * All events by Company
 */
Route::get('/company/events/{id}/{userID}', function (Request $request, $id, $userID) {
	$response = array(
		'data' => \App\Companies::getCompetitionsByCompanyID( $id ),
		'error' => 0
	);

	if(isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	if($userID <= 0) {
		$response['error'] = 1;
		return response($response, 200)
			->header('Content-Type', 'application/json');
	}

	foreach ($response['data'] as $comp) {
		$comp->com_is_enlisted = Participants::isUserEnlisted($userID, $comp->id);
	}

	return response($response, 200)
		->header('Content-Type', 'application/json');
});


/**
 * Get all EVENTS - or all companies who host competitions
 */
Route::get('/companies', function (Request $request) {
	$response = array(
		'data' => \App\Companies::getAll( 500 ),
		'error' => 0
	);

	if(isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	return response($response, 200)
		->header('Content-Type', 'application/json');
});


/**
 * Get information about a company (event)
 */
Route::get('/company/{id}', function (Request $request, $id) {
	$response = array(
		'data' => \App\Companies::getByID( $id),
		'error' => 0
	);

	if(isset($response['data']) && count($response['data']) <= 0 )
		$response['error'] = 1;

	return response($response, 200)
		->header('Content-Type', 'application/json');
});

/**
 * get User info - check if user exist by email, and verify password
 */
Route::post('/login', function (Request $request) {
	$email = strtolower($request->get('email'));
	$pw = $request->get('password');
	$user = \App\User::where('email', $email)->get();
	$isPWCorrect = false;
	if($user != null && $user->count() >0) {
		$user = $user->first();
		$isPWCorrect = \Illuminate\Support\Facades\Hash::check($pw, $user->password);
	}

	$data['user'] = $user;

	if($user == null || $isPWCorrect == false)
		$data['error'] = 1;
	else
		$data['error'] = 0;

	$response = array(
		'data' => $data
	);

	return response($response, 200)
		->header('Content-Type', 'application/json');
});

