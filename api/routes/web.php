<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\RouteCollection;

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
/***
 * dont call Auth::routes() facade, since we customize the routing now
 */
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('registerc', 'Auth\RegisterController@showRegistrationFormCompany')->name('registerc');

Route::post('register', 'Auth\RegisterController@register');
Route::post('registerc', 'Auth\RegisterController@registerCompany');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Email Verification Routes...
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');



Route::get('/home', 'HomeController@index')->name('home');

/* User Routes*/
Route::get('/profile', 'HomeController@profile')->name('profile'); //show profile
Route::post('/user/update', 'UserController@update');
Route::get('/user/competitions', 'UserController@competitions')->name('userComp');

/*Competition Routes*/
Route::get('/comp/index', 'CompetitionController@index')->name('compIndex');
Route::get('/comp/create', 'CompetitionController@create')->name('compCreate');
Route::get('/comp/edit', 'CompetitionController@edit')->name('compEdit');
Route::get('/comp/show', 'CompetitionController@show')->name('compShow');
Route::post('/comp/store', 'CompetitionController@store')->name('compStore');
Route::post('/comp/update', 'CompetitionController@update')->name('compUpdate');

Route::post('/comp/enlist', 'CompetitionController@enlist')->name('compEnlist');
Route::post('/comp/unlist', 'CompetitionController@unlist')->name('compUnlist');



/**********************************************************************************/
Route::get('/titles', function (\Illuminate\Http\Request $request){
	return view('randomTitles');
});

Route::get('/apiRoutes', function (\Illuminate\Http\Request $request) {
	if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->role >= ROLE_ADMIN) {
		$routes = new RouteCollection();
		$routes = Route::getRoutes()->getRoutes();

		return view('apiRoutes',
			['data' => array(
				'routes' => $routes
		)]);
	}else
		return redirect()->action('HomeController@index');
})->name('apiRoutes');