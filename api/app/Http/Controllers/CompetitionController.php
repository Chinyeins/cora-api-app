<?php

namespace App\Http\Controllers;

use App\Competitions;
use App\Participants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

define('NUM', 50);

class CompetitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$data = array(
    		'competitions' => Competitions::getAll(NUM)
	    );

        return view('competition.index', [
        	'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('competition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Competitions::saveNew($request)) {
        	$message = "Daten gespeichert";
        	$lastAdded = Competitions::getLastAddByUser(Auth::id());
        	return redirect()->action('CompetitionController@create', [
        		'success' => $message]
	        );
        } else {
	        $message = "Daten wurden nicht gespeichert. Bitte versuchen Sie es erneut";
	        return redirect()->action('CompetitionController@create',
		        [
		        	'sent' => $request->all(),
			        'error' => $message
		        ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = (isset($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
		$data = null;

        $data = Competitions::getByID($id);
		$subs = Participants::getSubScriptionCount($id);

        return view('competition.show', [
        	'comp' => $data,
	        'subs' => $subs
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
	    $id = ($request->get('id') != null) ? $request->get('id') : 0;
	    if(Auth::check() && Auth::user()->role >= ROLE_MODERATOR) {
		    $comp = Competitions::getByID($id);
		    return view('competition.edit',
			    ['competition' => $comp]);
	    } else {
	    	//redirect not auhtorized users
	    	return redirect()->action('CompetitionController@show',
			    [
			    	'id' => $id
			    ]);
	    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
	    if(Competitions::editOne($request)) {
		    $message = "Daten gespeichert";
		    return redirect()->action('CompetitionController@edit',
			    [
				    'success' => $message,
				    'id' => $request->get('com_id')
			    ]
		    );
	    } else {
		    $message = "Daten wurden nicht gespeichert. Bitte versuchen Sie es erneut";
		    return redirect()->action('CompetitionController@edit',
			    [
				    'sent' => $request->all(),
				    'error' => $message,
				    'id' => $request->get('com_id')
			    ]);
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        //
    }

	/**
	 * Asssign user to competition
	 *
	 */
	public function enlist(Request $request) {
    	if(Auth::check()) {
		    $userID = Auth::id();
		    $compID = $request->get('com_id');
		    if($compID <= 0) {
		    	//enlist error - no competition id given
			    $message = "Anmeldung zu Wettbewerb fehlgeschlagen. Keine Wettbewerbs ID angegeben";
			    return redirect()->action('CompetitionController@show',
				    [
					    'error' => $message,
					    'id' => $request->get('com_id')
				    ]
			    );
		    }

			$isSubscribbed = Participants::enList($userID, $compID);

			if($isSubscribbed === true) {
				$message = "Erfolgreich angemeldet";
				return redirect()->action('CompetitionController@show',
					[
						'success' => $message,
						'id' => $request->get('com_id')
					]
				);
			} else if($isSubscribbed === false) {
				$message = "Anmeldung zu Wettbewerb fehlgeschlagen. Bitte versuche es Erneut";
				return redirect()->action('CompetitionController@show',
					[
						'error' => $message,
						'id' => $request->get('com_id')
					]
				);
			} else if($isSubscribbed === null) {
				$message = "Anmeldung zu Wettbewerb fehlgeschlagen. Wettbewerb bereits voll!";
				return redirect()->action('CompetitionController@show',
					[
						'error' => $message,
						'id' => $request->get('com_id')
					]
				);
			}
	    } else {
    		//TODO: remember this action and force user to login first. Then commit action
    		return redirect()->action('HomeController@index');
	    }
	}

	/**
	 * Remove user from course
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|mixed
	 */
	public function unlist(Request $request) {
		if(Auth::check()) {
			$userID = Auth::id();
			$compID = $request->get('com_id');
			if($compID <= 0) {
				//enlist error - no competition id given
				$message = "Abmeldung zu Wettbewerb fehlgeschlagen. Keine Wettbewerbs ID angegeben";
				return redirect()->action('CompetitionController@show',
					[
						'error' => $message,
						'id' => $request->get('com_id')
					]
				);
			}

			$isUnSubscribbed = Participants::unList($userID, $compID);

			if($isUnSubscribbed) {
				$message = "Erfolgreich abgemeldet";
				return redirect()->action('CompetitionController@show',
					[
						'success' => $message,
						'id' => $request->get('com_id')
					]
				);
			} else {
				$message = "Abmeldung zu Wettbewerb fehlgeschlagen. Bitte versuche es Erneut";
				return redirect()->action('CompetitionController@show',
					[
						'error' => $message,
						'id' => $request->get('com_id')
					]
				);
			}
		} else {
			//TODO: remember this action and force user to login first. Then commit action
			return redirect()->action('HomeController@index');
		}
	}
}
