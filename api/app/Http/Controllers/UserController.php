<?php

namespace App\Http\Controllers;

use App\Competitions;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(User::editOne($request)) {
        	$message = "Daten gespeichert";
        	return redirect()->action('HomeController@profile', ['success' => $message]);
        } else {
	        $message = "Daten konnten nicht gespeichert werden";
	        return redirect()->action('HomeController@profile', ['error' => $message]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function competitions(Request $request)
    {
    	$d = null;
    	if(Auth::user()->role >= ROLE_MODERATOR)
    		$d = Competitions::getByEmployer();
    	else
    		$d = Competitions::getByUser();

	    $data = array(
		    'competitions' => $d
	    );

    	return view('user.competitions', ['data' => $data]);
    }
}
