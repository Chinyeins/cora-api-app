<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Participants extends Model implements IResponse
{
    //
	public static function saveNew( Request $request ) {
		// TODO: Implement saveNew() method.
	}

	public static function editOne( Request $request ) {
		// TODO: Implement editOne() method.
	}

	public static function getByID( $id ) {

	}

	public static function getAll( $num ) {
		// TODO: Implement getAll() method.
	}


	/**
	 * Add User to competition
	 *
	 * @param $userID
	 * @param $compID
	 *
	 * @return bool
	 */
	public static function enList($userID, $compID) {
		//enlisting is only allowed for normal users
		if(!self::isUserEnlisted($userID, $compID)) {
			if(self::isCompetitionFull($compID) == false) {
				$part = new Participants();
				$part->part_comp_id = $compID;
				$part->part_user_id = $userID;
				return $part->save();
			} else {
				return null;
			}
		}
	}

	/**
	 * Remove User from competition
	 *
	 * @param $userID
	 * @param $compID
	 *
	 * @return mixed
	 */
	public static function unList($userID, $compID) {
		return Participants::where([
			['part_comp_id', $compID],
			['part_user_id', $userID]
		])
			->delete();
	}

	public static function isUserEnlisted($userID, $compID) {
		$enlist = Participants::select('*')
			->where([
					['part_comp_id', $compID],
					['part_user_id', $userID]
				]
			)
			->get();

		if($enlist != null && $enlist->count() > 0)
			return true;
		else
			return false;
	}

	public static function getSubScriptionCount( $id ) {
		return Participants::where('part_comp_id', $id)->count();
	}

	public static function isCompetitionFull( $compID ) {
		$subs = self::getSubScriptionCount($compID);
		$comp = Competitions::find($compID);

		if(intval($subs) >= intval($comp->com_size)) {
			return true;
		} else {
			//is full or error
			return false;
		}
	}
}
