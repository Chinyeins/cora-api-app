<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

define('COMP_STATUS_PUBLIC', 0);
define('COMP_STATUS_PRIVATE', 1);

class Competitions extends Model implements IResponse
{
    //
	public static function saveNew( Request $request ) {
		$comp = new Competitions();
		$req = $request->all();

		$name = (isset($req['name'])) ? $req['name'] : '';
		$descr = (isset($req['descr'])) ? $req['descr'] : '';
		$catID = (isset($req['cat_id_filter'])) ? $req['cat_id_filter'] : '';
		$start = (isset($req['start'])) ? $req['start'] : '';
		$end = (isset($req['end'])) ? $req['end'] : '';
		$size = (isset($req['size'])) ? $req['size'] : 0;
		$companyID = User::hasCompany(Auth::id()); //use id from current user

		if($start != null)
			$start = Carbon::createFromFormat('Y-m-d', $start)->toDateString();
		$end = (isset($req['end'])) ? $req['end'] : '';
		if($end != null)
			$end = Carbon::createFromFormat('Y-m-d', $end)->toDateString();
		$status = (isset($req['status'])) ? $req['status'] : COMP_STATUS_PRIVATE; //set default to privat

		if(!empty($name) &&
		   !empty($descr) &&
		   !empty($catID) &&
		   !empty($start) &&
		   !empty($end) &&
		   $companyID != null && $companyID > 0) {
			$comp->com_name = $name;
			$comp->com_descr = $descr;
			$comp->com_category_id = $catID;
			$comp->com_start = $start;
			$comp->com_end = $end;
			$comp->com_status = $status;
			$comp->com_company_id = $companyID;
			$comp->com_size = $size;

			return $comp->save();
		} else {
			return false;
		}
	}

	public static function editOne( Request $request ) {
		$req = $request->all();
		$id = (isset($req['com_id'])) ? $req['com_id'] : 0;
		$comp = null;
		if($id > 0)
			$comp = Competitions::find($id);

		if($comp == null)
			return false;

		$name = (isset($req['name'])) ? $req['name'] : '';
		$descr = (isset($req['descr'])) ? $req['descr'] : '';
		$catID = (isset($req['cat_id_filter'])) ? $req['cat_id_filter'] : '';
		$start = (isset($req['start'])) ? $req['start'] : '';
		$end = (isset($req['end'])) ? $req['end'] : '';
		$size = (isset($req['size'])) ? $req['size'] : 0;
		$companyID = User::hasCompany(Auth::id()); //use id from current user

		if($start != null)
			$start = Carbon::createFromFormat('Y-m-d', $start)->toDateString();
		$end = (isset($req['end'])) ? $req['end'] : '';
		if($end != null)
			$end = Carbon::createFromFormat('Y-m-d', $end)->toDateString();
		$status = (isset($req['status'])) ? $req['status'] : COMP_STATUS_PRIVATE; //set default to privat

		if(!empty($name) &&
		   !empty($descr) &&
		   !empty($catID) &&
		   !empty($start) &&
		   !empty($end) &&
		   $companyID != null && $companyID > 0) {
			$comp->com_name = $name;
			$comp->com_descr = $descr;
			$comp->com_category_id = $catID;
			$comp->com_start = $start;
			$comp->com_end = $end;
			$comp->com_status = $status;
			$comp->com_company_id = $companyID;
			$comp->com_size = $size;

			return $comp->save();
		} else {
			return false;
		}
	}

	public static function getByID( $id ) {
		$data = null;
		if($id > 0) {
			$data = Competitions::select('*', 'competitions.id AS com_id')
				->where('competitions.id', $id)
                ->leftJoin('categories', 'competitions.com_category_id', 'categories.id')
				->leftJoin('companies', 'competitions.com_company_id', 'companies.id')
                ->get();
		}

		//add sub count
		if($data != null) {
			foreach ( $data as $comp ) {
				$comp->com_sub_count = Participants::getSubScriptionCount( $comp->id );
			}
		}

		if($data != null)
			$data = $data->toArray();

		return $data;
	}

	public static function getAll( $num ) {
		$comps = null;

		if($num != null && $num > 0) {
			$comps = Competitions::select('*', 'competitions.id AS com_id')
			                     ->leftJoin('categories','competitions.com_category_id',  'categories.id')
			                     ->get()
			                     ->take($num);
		} else {
			$comps = Competitions::select('*', 'competitions.id AS com_id')
			                     ->leftJoin('categories','competitions.com_category_id',  'categories.id')
			                     ->get();
		}


		foreach ($comps as $comp) {
			$comp->com_sub_count = Participants::getSubScriptionCount($comp->id);
		}

		return $comps;
	}

	//get all competitions by employe ID
	public static function getByEmployer() {
		//get company id from employe
		$companyID = User::hasCompany(Auth::id());
		$data = null;

		if($companyID != null && $companyID > 0) {
			$data = Competitions::where('competitions.com_company_id', $companyID)
						->get();
		}

		//add sub count
		if($data != null) {
			foreach ( $data as $comp ) {
				$comp->com_sub_count = Participants::getSubScriptionCount( $comp->id );
			}
		}
		return $data;
	}

	public static function getByUser() {
		$data = null;
		$userID = Auth::id();
		if($userID != null) {
			$data = Participants::where('part_user_id', $userID)
				->leftJoin('competitions', 'participants.part_comp_id', 'competitions.id')
				->get();
		}

		//add sub count
		if($data != null) {
			foreach ($data as $comp) {
				$comp->com_sub_count = Participants::getSubScriptionCount($comp->id);
			}
		}

		return $data;
	}

	public static function getByUserID($userID) {
		$data = null;
		if($userID != null) {
			$data = Participants::where('part_user_id', $userID)
			                    ->leftJoin('competitions', 'participants.part_comp_id', 'competitions.id')
			                    ->get();
		}

		//add sub count
		if($data != null) {
			foreach ($data as $comp) {
				$comp->com_sub_count = Participants::getSubScriptionCount($comp->id);
			}
		}

		return $data;
	}

	public static function getLastAddByUser( $id ) {
		if($id != null && $id > 0) {
			$companyID = User::hasCompany($id);
			if($companyID != null && $companyID > 0) {
				$d = Competitions::where('com_company_id', $companyID)->get();
				if($d != null && $d->count() > 0)
					return $d->get($d->count()-1);
			}
		}
	}
}
