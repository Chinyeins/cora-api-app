<?php
/**
 * Created by PhpStorm.
 * User: Christopher
 * Date: 03.10.2018
 * Time: 21:01
 */

namespace App;

use Illuminate\Http\Request;

interface IResponse {
	public static function saveNew(Request $request);
	public static function editOne(Request $request);
	public static function getByID($id);
	public static function getAll($num);
}