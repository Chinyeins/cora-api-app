<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Companies extends Model implements IResponse
{
	protected $table = 'companies';
    //
	public static function saveNew( Request $request ) {
		// TODO: Implement saveNew() method.
	}

	public static function editOne( Request $request ) {
		// TODO: Implement editOne() method.
	}

	public static function getByID( $id ) {
		$comp = Companies::find($id);

		if($comp != null) {
			return $comp->toArray();
		}
	}

	public static function getAll( $num ) {
		return Companies::select('*')->get();
	}

	public static function getByUserID( $id ) {
		$company = CompaniesUsers::where('cu_user_id', $id)
			->leftJoin('companies', 'companies_users.cu_company_id', 'companies.id')
			->get();

		if($company != null) {
			return $company->get(0);
		}
	}

	public static function getCompetitionsByCompanyID( $compID ) {
		$data = null;
		if($compID != null) {
			$data = Competitions::where('com_company_id', $compID)->get();
		}

		//add sub count
		if($data != null) {
			foreach ($data as $comp) {
				$comp->com_sub_count = Participants::getSubScriptionCount($comp->id);
			}
		}

		return $data;
	}
}
