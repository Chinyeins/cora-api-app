<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

//ROLES CONSTANTS
define('ROLE_ADMIN', 3);
define('ROLE_EMPLOYER', 2);
define('ROLE_MODERATOR', 1);
define('ROLE_USER', 0);

class User extends Authenticatable implements IResponse
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	public static function saveNew( Request $request ) {
		// TODO: Implement saveNew() method.
	}

	public static function editOne( Request $request ) {
		$user = User::find(Auth::id());
		$req = $request->all();

		$name = (isset($req['name'])) ? $req['name'] : null;
		$surname = (isset($req['surname'])) ? $request->get('surname') : '';
		$lastname = (isset($req['lastname'])) ? $req['lastname'] : '';

		$street = (isset($req['street'])) ? $req['street'] : '';
		$street_nr = (isset($req['street_nr'])) ? $req['street_nr'] : '';
		$postcode = (isset($req['postcode'])) ? $req['postcode'] : '';

		$company_img_src = (isset($req['img_src'])) ? $req['img_src'] : '';
		//hack
		$compOFUser = Companies::getByUserID(Auth::id());
		if($compOFUser != null) {
			$compID = $compOFUser->cu_company_id;
			$company = Companies::find($compID);
			if($company != null) {
				$company->co_img_src = $company_img_src;
				$company->save();
				$compOFUser = null;
			}
		}

		if(!empty($name)) {
			$user->name = $name;
			$user->surname = $surname;
			$user->lastname = $lastname;

			$user->street = $street;
			$user->street_nr = $street_nr;
			$user->postcode = $postcode;

			return $user->save();
		} else {
			return false;
		}
	}

	public static function getByID( $id ) {
		return User::where('id', $id)->get();
	}

	public static function getAll( $num ) {
		// TODO: Implement getAll() method.
	}

	public static function role() {
		$user = User::find(Auth::id());
		if($user != null)
			return $user->role;
	}

	/**
	 * Get company ID from user, assuming he belongs to a company.
	 *
	 * If not return null
	 */
	public static function hasCompany($userID) {
		if($userID == null)
			return null;

		$id = CompaniesUsers::where('cu_user_id', $userID)->get();
		if($id != null && $id->count() > 0) {
			$id = $id->get(0);
			$id = $id->cu_company_id;
		} else {
			$id = null;
		}

		return $id;
	}

	public static function getCompanyName() {
		$compID = self::hasCompany(Auth::id());
		if($compID) {
			$name = Companies::find($compID);
			if($name != null) {
				return $name->co_name;
			}
		}
	}
}
