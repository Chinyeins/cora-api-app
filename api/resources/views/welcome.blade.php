@extends('layouts.app')

@section('headerImage')
    <div class="header-image" style=" z-index: -10;background-image: url('{{asset('/images/landinpage1.jpg')}}')"></div>

@endsection

@section('content')
    <section id="landin_page" class="container header-title">
        <h1>Das Competition Radar <br>
            <a href="{{action('CompetitionController@index')}}" class="simple-btn simple-btn-sm">Finde deinen Wettbewer!</a>
        </h1>
    </section>
    <div class="clearfix"></div>
    <section class="content">
        <article class="container">
            <br>
            <br>
            <h3>Finden Sie jetzt den passenden Wettbewerb<br> für Ihre Fähigkeiten</h3>
            <hr>
            <br>
            <h4>Was wir bieten</h4>
            <br>
            <div class="teaser">
                <div class="clearfix"></div>
                <div class="bubble-inline">
                    <div class="bubble-align">
                        <div class="bubble">
                            <span>Kostenlose<br> Registrierung</span>
                        </div>
                    </div>
                    <div class="bubble-align">
                        <div class="bubble">
                            <span>Begehrte<br> Preise</span>
                        </div>
                    </div>
                    <div class="bubble-align">
                        <div class="bubble">
                            <span>Chancen für<br>Ihre Karriere</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <br>
                <hr>
                <br>

            </div>
            <h3>Es ist ganz einfach<br>registrieren Sie sich jetzt!</h3>
            <br>
            <br>
            <div class="choice_register">
                <div class="background-pic choice" style="background-image: url('{{asset('/images/people_register.jpg')}}')">
                    <a href="{{ route('register') }}" class="simple-btn">Als Teilnehmer registrieren</a>
                </div>
                <div class="background-pic choice" style="background-image: url('{{asset('/images/company_register.jpg')}}')">
                    <a href="{{ route('registerc') }}" class="simple-btn">Unternehmen registrieren</a>
                </div>
            </div>
        </article>
    </section>
    <!--<section>
        <article class="header-title">
            <div style="background-image: url('{{asset('/images/back1.jpg')}}')"></div>

            <h5>Wählen Sie aus den verschiedenen Kategorien:
                @php $numC = 0;
                    $categories = \App\Categories::getAll(50);
                    foreach($categories as $key => $cat) {
                        echo $cat->c_name;

                        if($numC < count($categories)-1)
                            echo ', ';
                        else
                            echo '.';
                        $numC++;
                    }
                @endphp
            </h5>
        </article>
    </section>-->
@endsection