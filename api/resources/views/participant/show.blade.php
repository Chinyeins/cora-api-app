@extends('layouts.app')

@section('content')
    @include('utility.alert')
    <br>
    @if($comp == null && isset($comp[0]) )
        <div class="container">
            <div class="alert alert-primary">
                Der Wettbewerb mit dieser ID existiert nicht
            </div>
        </div>
    @else
    <section class="container">
        <h1>Wettbewerbsübersicht</h1>
        <hr>
        <h2>Titel: {{$comp[0]['com_name']}}</h2>
        <br>
        <div class="comp_show_page">
            <p><strong>Kategory:</strong> <span>{{$comp[0]['c_name']}}</span></p>
        </div>
        <div class="comp_show_page">
            <p><strong>Beschreibung:</strong></p>
            <textarea rows="8" class="form-control">{{$comp[0]['com_descr']}}</textarea>
        </div>
        <br>
        <div class="comp_show_page">
            <p><strong>Beginn:</strong> <span>{{\Carbon\Carbon::createFromTimeString($comp[0]['com_start'])->format('d.m.Y') }}</span></p>
            <p><strong>Ende:</strong> <span>{{\Carbon\Carbon::createFromTimeString($comp[0]['com_end'])->format('d.m.Y') }}</span></p>
        </div>
    </section>
    @endif
@endsection