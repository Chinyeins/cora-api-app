@extends('layouts.app')

@section('content')
    @include('utility.alert')
    <br>
    <section class="container">
        <h1>Neuen Wettbewerb erstellen</h1>

        <hr>
        <form action="{{action('CompetitionController@store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label>Titel</label>
                <input class="form-control" type="text" name="name">
            </div>

            <div class="form-group">
                <label>Kategorie</label><br>
                @include('category.filter')
            </div>

            <div class="form-group">
                <label>Beschreibung</label>
                <textarea class="form-control" name="descr"></textarea>
            </div>

            <div class="form-group">
                <label>Beginn</label>
                <input class="form-control" type="date" name="start">
            </div>

            <div class="form-group">
                <label>Ende</label>
                <input class="form-control" type="date" name="end">
            </div>

            <div class="form-group">
                <label>Status</label><br>
                <select class="form-control-sm" name="status">
                    <option value="0">Öffentlich</option>
                    <option value="1">Privat / Geschlossen</option>
                </select>
            </div>
            <br>
            <br>
            <input class="btn btn-success" type="submit" name="submit">
        </form>
    </section>
@endsection