@extends('layouts.app')



@section('content')
    @include('utility.alert')
    <br>
    <section class="container">
        <h2>Meine Daten</h2>
        <hr>
        @php
            $company = \App\Companies::getByUserID(\Illuminate\Support\Facades\Auth::id());
        @endphp
        @if(\Illuminate\Support\Facades\Auth::user()->role == ROLE_EMPLOYER)

            <form action="{{action('UserController@update')}}" method="POST">
                @csrf
                @if($company != null)
                <div class="form-group">
                    <label><strong>Unternehmen</strong></label>
                    <h4>{{$company->co_name}}</h4>
                </div>
                <div class="company-img">
                    <img src="{{$company->co_img_src}}" alt="" />
                </div>
                <div class="form-group">
                    <label><b>Firmenlogo: URL</b></label>
                    <input id="img_src" type="text" class="form-control" name="img_src" value="{{$company->co_img_src}}">
                </div>
                @else
                    <div class="alert alert-primary">
                        Noch keinem Unternehmen zugeordnet
                    </div>
                @endif
                <hr>
                <div class="form-group">
                    <label><strong>Benutzername</strong></label>
                    <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}">
                </div>

                <div class="form-group">
                    <label><strong>Vorname</strong></label>
                    <input type="text" class="form-control" name="surname" value="{{$user->surname}}"/>
                </div>

                <div class="form-group">
                    <label><strong>Nachname</strong></label>
                    <input type="text" class="form-control" name="lastname" value="{{$user->lastname}}">
                </div>
                <hr>
                <h3>Anschrift</h3>
                <div class="form-group">
                    <label><strong>Straße</strong></label>
                    <input type="text" class="form-control" name="street" value="{{$user->street}}">
                </div>
                <div class="form-group">
                    <label><strong>Nr.</strong></label>
                    <input type="number" class="form-control" name="street_nr" value="{{$user->street_nr}}">
                </div>
                <div class="form-group">
                    <label><strong>Postleitzahl</strong></label>
                    <input type="number" class="form-control" name="postcode" value="{{$user->postcode}}">
                </div>

                <br>
                <br>
                <input class="btn btn-success" type="submit" name="submit" value="Speichern">
            </form>
        @else
            <form action="{{action('UserController@update')}}" method="POST">
                @csrf
                @if($company != null)
                    <div class="form-group">
                        <label><strong>Unternehmen</strong></label>
                        <h4>{{$company->co_name}}</h4>
                    </div>
                    <div class="company-img">
                        <img src="{{$company->co_img_src}}" alt="" />
                    </div>
                    <div class="form-group">
                        <label><b>Firmenlogo: URL</b></label>
                        <input id="img_src" type="text" class="form-control" name="img_src" value="{{$company->co_img_src}}">
                    </div>
                @else
                    <div class="alert alert-primary">
                        Noch keinem Unternehmen zugeordnet
                    </div>
                @endif
                <div class="form-group">
                    <label><strong>Benutzername</strong></label>
                    <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}">
                </div>

                <div class="form-group">
                    <label><strong>Vorname</strong></label>
                    <input type="text" class="form-control" name="surname" value="{{$user->surname}}"/>
                </div>

                <div class="form-group">
                    <label><strong>Nachname</strong></label>
                    <input type="text" class="form-control" name="lastname" value="{{$user->lastname}}">
                </div>

                <hr>
                <h3>Anschrift</h3>
                <div class="form-group">
                    <label><strong>Straße</strong></label>
                    <input type="text" class="form-control" name="street" value="{{$user->street}}">
                </div>
                <div class="form-group">
                    <label><strong>Nr.</strong></label>
                    <input type="number" class="form-control" name="street_nr" value="{{$user->street_nr}}">
                </div>
                <div class="form-group">
                    <label><strong>Postleitzahl</strong></label>
                    <input type="number" class="form-control" name="postcode" value="{{$user->postcode}}">
                </div>

                <br>
                <br>
                <input class="btn btn-success" type="submit" name="submit" value="Speichern">
            </form>
        @endif
    </section>
@endsection
