<!--categories-->
@php
$categories_filter = \App\Categories::getAll(50);
@endphp
<select name="cat_id_filter" class="form-control-sm">
    @foreach($categories_filter as $cat_fi)
        @if(isset($_REQUEST['cat_id']) && $_REQUEST['cat_id'] == $cat_fi->id)
            <option selected value="{{$cat_fi->id}}">{{$cat_fi->c_name}}</option>
        @else
            <option value="{{$cat_fi->id}}">{{$cat_fi->c_name}}</option>
        @endif
    @endforeach
</select>