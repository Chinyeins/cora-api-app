@extends('layouts.app')

@section('content')
    <section class="container">
        <h1>Alle Routes im Backend</h1>
        <br>
        <table class="table table-bordered">
            <tr>
                <td><strong>Route Name</strong></td>
                <td><strong>Route Link</strong></td>
                <td><strong>Methods</strong></td>
            </tr>
            @if($data['routes'])
                @foreach($data['routes'] as $route)
                    <tr>
                        <td>
                            <strong>{{$route->uri}}</strong>
                        </td>
                        <td>
                            <a href="{{url('/').'/'. $route->uri}}">{{$route->uri}} </a>
                        </td>
                        <td>
                            @foreach($route->methods as $method)
                                {{$method }}
                            @endforeach
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </section>
@endsection