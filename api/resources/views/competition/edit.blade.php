@extends('layouts.app')

@section('content')
    @include('utility.alert')
    <br>
    <section class="container">
        @if(isset($competition[0]))
            @php
                $competition = $competition[0];
            @endphp
            <h1>Bearbeiten <br>"{{$competition['com_name']}}"</h1>
            @php
                $id = \App\Competitions::getLastAddByUser(\Illuminate\Support\Facades\Auth::id());
            @endphp
            @if(isset($_REQUEST['last_added']))

                <a href="{{route('compShow').'?id='.$_REQUEST['last_added_id']}}">Zuletzt erstellt: {{$_REQUEST['last_added']}}</a>
            @endif

            @if(\App\User::hasCompany(\Illuminate\Support\Facades\Auth::id()) <= 0)
                <div class="alert alert-primary">
                    Sie sind keinem Unternehmen zugeordnet. Das Erstellen eines Wettbewerbes ist daher nicht möglich.
                </div>
            @else
                <hr>
                <form action="{{action('CompetitionController@update')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Titel</label>
                        <input class="form-control" type="text" name="name" value="{{$competition['com_name']}}">
                    </div>

                    <div class="form-group">
                        <label>Kategorie</label><br>
                        @php
                            $_REQUEST['cat_id'] = $competition['com_category_id'];
                        //dd($competition);
                        @endphp
                        @include('category.filter')
                    </div>

                    <div class="form-group">
                        <label>Beschreibung</label>
                        <textarea class="form-control" name="descr">{{$competition['com_descr']}}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Beginn</label>
                        <input class="form-control" type="date" name="start" value="{{\Carbon\Carbon::parse($competition['com_start'])->format('Y-m-d') }}">
                    </div>

                    <div class="form-group">
                        <label>Ende</label>
                        <input class="form-control" type="date" name="end" value="{{\Carbon\Carbon::parse($competition['com_end'])->format('Y-m-d') }}">
                    </div>

                    <div class="form-group">
                        <label>Status</label><br>
                        <select class="form-control-sm" name="status">
                            @if($competition['com_status'] == 0)
                                <option selected value="0">Öffentlich</option>
                                <option value="1">Privat / Geschlossen</option>
                            @else
                                <option value="0">Öffentlich</option>
                                <option selected value="1">Privat / Geschlossen</option>
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Teilnehmerzahl</label>
                        <input class="form-control" type="number" name="size" max="999999" value="{{$competition['com_size']}}">
                    </div>
                    <br>
                    <br>
                    <input type="hidden" name="com_id" value="{{$competition['com_id']}}">
                    <input class="btn btn-success" type="submit" name="submit" value="Speichern">
                </form>
            @endif
        @else
            <div class="alert alert-primary">
                Der Wettbewerb mit der angegebenen ID existiert nicht
            </div>
        @endif
    </section>

@endsection