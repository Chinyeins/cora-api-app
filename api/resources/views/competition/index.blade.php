@extends('layouts.app')

@php
use App\User;
use App\Competitions;
@endphp

@section('content')
    @include('utility.alert')
    <br>
    <section class="container">
        <h1>Alle Wettbewerbe</h1>

        @if(\App\User::role() >= ROLE_MODERATOR)
            <a style="float: right" class="btn btn-success" href="{{route('compCreate')}}">
                Neuer Wettbewerb
            </a>
            <div class="clearfix"></div>
        @endif
        <br>
        <div class="filter">
            @include('category.filter')
        </div>
        <br>
        @if(isset($data['competitions']) && $data['competitions']->count() > 0)
            <table class="table">
                <tr>
                    <td><strong>Titel</strong></td>
                    <td><strong>Kategorie</strong></td>
                    <td><strong>Begin</strong></td>
                    <td><strong>Ende</strong></td>
                    <td><strong>Teilnehmer</strong></td>
                    <td><strong>Status</strong></td>
                </tr>
                @foreach($data['competitions'] as $competition)
                    <tr>
                        <td><strong><a href="{{route('compShow').'?id='.$competition->com_id}}">{{$competition->com_name}}</a></strong></td>
                        <td>{{$competition->c_name}}</td>
                        <td> {{ \Carbon\Carbon::createFromTimeString($competition->com_start)->format('d.m.Y') }} </td>
                        <td> {{ \Carbon\Carbon::createFromTimeString($competition->com_end)->format('d.m.Y') }} </td>
                        <td>
                            {{ $competition->com_sub_count }}
                            /
                            {{ $competition->com_size }}
                        </td>
                        <td>
                            @if($competition->com_status == COMP_STATUS_PUBLIC)
                                Öffentlich
                            @else
                                Privat
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <div class="alert alert-primary">
                Keine Wettbewerbe gefunden
            </div>
        @endif
    </section>
@endsection