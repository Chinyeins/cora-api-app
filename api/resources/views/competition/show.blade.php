@extends('layouts.app')

@section('content')
    @include('utility.alert')
    <br>
    @if($comp == null || !isset($comp[0])  )
        <div class="container">
            <div class="alert alert-primary">
                Der Wettbewerb mit dieser ID existiert nicht
            </div>
        </div>
    @else
    <section class="container">
        <h1>
            <a href="{{route('compIndex')}}" style="font-size: 0.5em;float:left">&larr; Alle Wettbewerbe</a> <br><p>Details</p>
            @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->role >= ROLE_MODERATOR)
                <a href="{{route('compEdit').'?id='.$comp[0]['com_id']}}" class="btn btn-warning">Edit</a><br>
            @endif
        </h1>
        <hr>
        <h2>{{$comp[0]['com_name']}}</h2>
        <br>
        <table class="table">
            <tr>
                <td><strong>Unternehmen:</strong> </td>
                <td class="">
                    <p><span>{{$comp[0]['co_name']}}</span></p>
                </td>
            </tr>
            <tr>
                <td><strong>Kategory:</strong></td>
                <td class="">
                    <p> <span>{{$comp[0]['c_name']}}</span></p>
                </td>
            </tr>
            <tr>
               <td><strong>Beschreibung:</strong></td>
                <td class="">
                    @if(empty($comp[0]['com_descr']))
                        <p>Keine Beschreibung vorhanden</p>
                    @else
                        <textarea rows="8" class="form-control">{{$comp[0]['com_descr']}}</textarea>
                    @endif
                </td>
            </tr>
            <tr>
                <td><strong>Beginn:</strong> </td>
                <td class="">
                    <p><span>{{\Carbon\Carbon::createFromTimeString($comp[0]['com_start'])->format('d.m.Y') }}</span></p>
                </td>
            </tr>
            <tr>
                <td><strong>Ende:</strong></td>
                <td>
                    <p> <span>{{\Carbon\Carbon::createFromTimeString($comp[0]['com_end'])->format('d.m.Y') }}</span></p>
                </td>
            </tr>
            <tr>
                <td><strong>Anmeldungen</strong></td>
                <td>
                    {{$subs}}
                    /
                    {{$comp[0]['com_size']}}
                </td>
            </tr>
        </table>
    </section>
        @if(\Illuminate\Support\Facades\Auth::check())
            <section class="container">
                @if(\App\Participants::isUserEnlisted(\Illuminate\Support\Facades\Auth::id(), $comp[0]['com_id']))
                    <form method="POST" action="{{action('CompetitionController@unlist')}}">
                        @csrf
                        <input type="hidden" name="com_id" value="{{$comp[0]['com_id']}}">
                        <input class="btn btn-danger" type="submit" name="enlist" value="Abmelden">
                    </form>
                @else
                    <form method="POST" action="{{action('CompetitionController@enlist')}}">
                        @csrf
                        <input type="hidden" name="com_id" value="{{$comp[0]['com_id']}}">
                        <input class="btn btn-success" type="submit" name="enlist" value="Anmelden">
                    </form>
                @endif
            </section>
        @endif
    @endif
@endsection