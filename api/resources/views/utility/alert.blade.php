<!--check for response messages-->
<section class="response-alert container">
    @if(isset($_REQUEST['success']) && !empty($_REQUEST['success']))
        <div class="alert alert-success">
           {{$_REQUEST['success']}}
        </div>
    @elseif(isset($_REQUEST['error']) && !empty($_REQUEST['error']))
        <div class="alert alert-danger">
          {{$_REQUEST['error']}}
        </div>
    @endif
</section>