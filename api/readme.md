#CORA - Competition Rating API
###Author: Christopher-Robin Fey


##Projektinstallation
Das Projekt ist eine Laravel Webapp. Das bedeutet damit diese Anwendung korrekt funktioniert
müssen folgende Programme installiert werden:

####Windows:
- Node.js (die ausführbare Exe muss zwingend in den PATH)
- Composer (composer muss ebenfalls in den PATH. Sie docu von composer)
- PHP (min. Version 5)
- XAMPP (Appache Server + SQL Datenbank)


###Projekt starten
1). Projekt in Lokalen erreichbar machen (htdocs)
Nachdem die benötigten Programme installiert sind, kopieren Sie das Projekt in einen Ordner ihres lokalen Testservers.
Eine Symbolische Verzeichnisverknüpfung ist auch möglich, falls sie das Projekt nicht direkt in ihrem htdocs Ordner des Testservers speichern wollen.

2). Installieren Sie die Node module über das Kommando:
`npm install`
Dazu müssen sie sich über das Terminal in das Projektverzeichnis begeben

2). Installieren Sie die übrigen Module:
`composer install`
Dazu müssen sie sich über das Terminal in das Projektverzeichnis begeben

3). Erstellen Sie eine neue Datenbank
Erstellen Sie in in Ihrem Datenbanksystem eine neue Datenbank mit dem Namen cora. Zum aufsetzen der tabellen geben Sie bitte über die Kommandozeile folgenden Befehl ein:
`php artisan migrate`

4). Server starten und Datenbank
Starten sie den Server und Ihre Datenbank und rufen sie die adresse:
`localhost/<pfad_zum_project_verzeichnis>/public`
auf. Die Anwendung ist nun bereit anfragen zu bearbeiten.


#CORA - Competition Radar Website + API
###Author: Christopher-Robin Fey


##Setup
This project contains an Laravel 5 framework app. This means in order to run the website and the API you´ll need to install some packages first:

####Windows:
- Node.js (please isntall node.js if you haven´t already and make sure you add the node.exe to your PATH !)
- Composer (install composer and make sure it´s also accessible from anywhere on the command prompt by putting it into the PATH too. Don´t know how to do that? Check out the composers officially documentation)
- PHP (min. Version 5)
- XAMPP (Appache Server + SQL database)


###Run the app
1). After downloading this project, make sure the api project files are either inside the XAMPP htdocs folder or you created a symlink to this files into the htdocs folder.
By doing this, the app will be served by the apache server from xampp.

2). Installing the node modules:
Open a command prompt. Cd into the api projects root folder. Type the following command to install all modules you need.
`npm install`

2). Installing the php packages via the php package manager "composer":
Open a command prompt. Cd into the api projects root folder. Type the following command to install all modules you need.
`composer install`

3). Create a new database:
Log into your database of choice on your machine. Create a new database called: "cora"
After that you need to setup all tables and seed the database with dummy data. To do this you simply cd into the api´s root folder and enter the following command into the terminal.
`php artisan migrate --seed`
This will run all migrations and after that, fill the tables with fake data. Don´t worry if the seeding takes a while.

4). Starting the apache server and datbase if you haven´t already
Just make sure the apache server is running and the database is too. Then browse to the url:
`http://localhost/<path_to_your_api_folder_on_your_machine>/public`
and bam. Here you go :)

