<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompetitions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('competitions', function (Blueprint $table) {
		    $table->increments('id');

			$table->char('com_name', '100');
			$table->longText('com_descr');
			$table->integer('com_category_id')->unsigned();
		    $table->integer('com_status')->unsigned();
		    $table->integer('com_company_id')->unsigned();

			$table->dateTime('com_start');  //startzeit
		    $table->dateTime('com_end');    //endzeit
		    $table->integer('com_size')->unsigned()->nullable();

		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('competitions');
    }
}
