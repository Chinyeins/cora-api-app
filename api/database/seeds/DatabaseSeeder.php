<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(UserSeeder::class); //create standard users
	    $this->call(CategorySeeder::class); //create standard categories
	    $this->call(CompanySeeder::class);
	    $this->call(CompetitionsSeeder::class);
	    $this->call(ParticipantsSeeder::class);

    }
}
