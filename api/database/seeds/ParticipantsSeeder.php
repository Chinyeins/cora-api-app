<?php

use Illuminate\Database\Seeder;

class ParticipantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    /**
	     * Enlist users randomly to competitions
	     */
		$courseCount = \App\Competitions::count();
		for($i = 1; $i <= $courseCount; $i++) {
			$comp = \App\Competitions::find($i);
			$size = $comp->com_size;

			//define fake competition subscriptions
			$fakeSubs = mt_rand(10, $size);
			for($j = 1; $j <= $fakeSubs; $j++) {
				self::enlistUserStatic( ($j+4), $comp->id );
			}
		};
    }

	private static function enlistUserStatic($userID, $competitionID) {
		//enlisting is only allowed for normal users
		if(!\App\Participants::isUserEnlisted($userID, $competitionID)) {
			if(\App\Participants::isCompetitionFull($competitionID) == false) {
				$part = new \App\Participants();
				$part->part_comp_id = $competitionID;
				$part->part_user_id = $userID;
				return $part->save();
			} else {
				return null;
			}
		}
	}
}
