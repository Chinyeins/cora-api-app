<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//create ADMIN USER
	    $chris = new \App\User();
	    $chris->name = "ChrisRob";
	    $chris->lastname = "Fey";
	    $chris->surname = "Christopher-Robin";
	    $chris->street = "Sankt Petersburg";
	    $chris->street_nr = "2";
	    $chris->postcode = "88888";
	    $chris->email = "chiny1@web.de";
	    $chris->password = "$2y$10\$HZbOw.JGdmmDthUNcVi7G.5CJyB6aAPYrxyAEJFnXSXeqxJsweY8y";//PW: 123456
		$chris->role = ROLE_ADMIN;

	    $chris->save();

	    //create ADMIN USER
	    $tom = new \App\User();
	    $tom->name = "TristanW";
	    $tom->lastname = "Tristan";
	    $tom->surname = "Walter";
	    $tom->street = "";
	    $tom->street_nr = "1";
	    $tom->postcode = "88888";
	    $tom->email = "tristan.w@web.de";
	    $tom->password = "$2y$10\$HZbOw.JGdmmDthUNcVi7G.5CJyB6aAPYrxyAEJFnXSXeqxJsweY8y"; //PW: 123456
	    $tom->role = ROLE_ADMIN;

	    $tom->save();

	    //add admins to company
	    $cu1 = new \App\CompaniesUsers();
	    $cu1->cu_company_id = 1;
	    $cu1->cu_user_id = 1;
	    $cu1->save();

	    $cu2 = new \App\CompaniesUsers();
	    $cu2->cu_company_id = 1;
	    $cu2->cu_user_id = 2;
	    $cu2->save();

	    /********
	    ADDITIONALLY ADD BOTH ADMINS TO COMPANY with ID 1. just random decision,
	     * to make admins able to submit to competitions
	     ******/


	    //create fake Company for testing
	    $companyFake = new \App\Companies();
	    $companyFake->co_street = "Streetname";
	    $companyFake->co_street_nr = "8";
	    $companyFake->co_postcode = "88888";
	    $companyFake->co_name = "CRF-Webdesign";
	    $companyFake->save();

	    //create MODERATOR (COMPANY) User
	    $emp1 = new \App\User();
	    $emp1->name = "ChrisRob_Employer";
	    $emp1->lastname = "Fey";
	    $emp1->surname = "Christopher-Robin";
	    $emp1->street = "Streetname";
	    $emp1->street_nr = "46";
	    $emp1->postcode = "88888";
	    $emp1->email = "employer@web.de";
	    $emp1->password = "$2y$10\$HZbOw.JGdmmDthUNcVi7G.5CJyB6aAPYrxyAEJFnXSXeqxJsweY8y";//PW: 123456
	    $emp1->role = ROLE_EMPLOYER;

	    $emp1->save();

	    //create ADMIN USER
	    $emp2 = new \App\User();
	    $emp2->name = "ChrisRob_User";
	    $emp2->lastname = "Fey";
	    $emp2->surname = "Christopher-Robin";
	    $emp2->street = "Streetname";
	    $emp2->street_nr = "46";
	    $emp2->postcode = "88888";
	    $emp2->email = "user@web.de";
	    $emp2->password = "$2y$10\$HZbOw.JGdmmDthUNcVi7G.5CJyB6aAPYrxyAEJFnXSXeqxJsweY8y";//PW: 123456
	    $emp2->role = ROLE_USER;

	    $emp2->save();

	    //assign fake employes to fake company
	    $fake1 = new \App\CompaniesUsers();
	    $fake1->cu_company_id = $companyFake->id;
	    $fake1->cu_user_id = $emp1->id;
	    $fake1->save();

	    $fake2 = new \App\CompaniesUsers();
	    $fake2->cu_company_id = $companyFake->id;
	    $fake2->cu_user_id = $emp2->id;
	    $fake2->save();

	    $this->generateRandomUser(500123);
    }

    public function generateRandomUser($limit) {
		$randomNames = json_decode('[
			["Katharyn", "Cline", " "],
			["Caren", "Reynaga", " "],
			["Luetta", "Gibney", " "],
			["Morgan", "Auvil", " "],
			["Hanna", "Riney", " "],
			["Adrian", "Horsley", " "],
			["Coreen", "Mares", " "],
			["Seema", "Utterback", " "],
			["Winnie", "Bisignano", " "],
			["Rosa", "Ruston", " "],
			["Lala", "Edgin", " "]
		]');

		$names = $this->pc_array_power_set($randomNames);

		for($i = 1; $i <= count($names)-1; $i++) {
			$ranID = $i+2;
			//new name
			$name = $names[$i];

			$user = new \App\User();
			$user->name = $name[0][0].'_'.$ranID;
			$user->lastname = $name[0][1];
			$user->surname = $name[0][0];
			$user->street = "Streetname";
			$user->street_nr = "46";
			$user->postcode = "88888";
			$user->email = $name[0][1].'.'.$name[0][0].$ranID.'@web.de';
			$user->password = "$2y$10\$Pk4SLnQQhYZYCneW7B5rD.9QZjc8XREjUW9yazah3y3BcIKb7DUye";
			$user->role = ROLE_USER;

			$user->save();
		}
    }

	/**
	 * Create the powerset of an array (every combination possible with an array of elements)
	 *
	 * @param $array
	 *
	 * @return array
	 */
	private function pc_array_power_set($array) {
		// initialize by adding the empty set
		$results = array(array( ));

		foreach ($array as $element)
			foreach ($results as $combination)
				array_push($results, array_merge(array($element), $combination));

		return $results;
	}
}
