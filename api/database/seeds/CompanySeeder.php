<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$size = 250;
		$chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'];
		$charRandCount = 0;
		
		for($i = 1; $i<=$size; $i++) {
			//create fake Company for testing
			$companyFake = new \App\Companies();
			$companyFake->co_name = "Event " . $i;
			$companyFake->co_street = "StreetName";
			$companyFake->co_street_nr = "7";
			$companyFake->co_postcode = "88888";
			
			if($charRandCount >= count($chars))
				$charRandCount = 0;
			
			$companyFake->co_img_src = "https://loremflickr.com/320/240/".$i;
			$charRandCount++;
			$companyFake->save();
		}
    }
}
