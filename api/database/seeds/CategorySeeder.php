<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$c1 = new \App\Categories();
		$c1->c_name = "Architektur";

	    $c2 = new \App\Categories();
	    $c2->c_name = "IT";

	    $c3 = new \App\Categories();
	    $c3->c_name = "Kunst";

	    $c4 = new \App\Categories();
	    $c4->c_name = "Medien";

	    $c5 = new \App\Categories();
	    $c5->c_name = "Musik";

	    $c6 = new \App\Categories();
	    $c6->c_name = "Wirtschaft";

	    $c1->save();
	    $c2->save();
	    $c3->save();
	    $c4->save();
	    $c5->save();
	    $c6->save();
    }
}
