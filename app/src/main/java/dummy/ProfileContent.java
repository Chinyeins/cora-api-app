

import org.json.JSONObject;

public class ProfileContent {
    public static final String profileData = "{'data':{'id':1,'name':'ChrisRob','email':'chiny1@web.de','email_verified_at':null,'created_at':'2018-11-17 17:59:17','updated_at':'2018-11-17 17:59:17','surname':'Christopher-Robin','lastname':'Fey','role':3,'street':'St. Pauls','street_nr':7,'postcode':88888},'error':0}";

    public static final JSONObject getProfile() {
        JSONObject profile = null;

        try {
            profile = new JSONObject(profileData);
        } catch (Exception e) {
            //
        }

        return profile;
    }
}
