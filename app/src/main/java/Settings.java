package ;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Settings {
    private static final String TAG = "Settings";
    private static Settings instance;
    private Settings() {

    }

    public static Settings getInstance() {
        if(instance == null) {
            instance = new Settings();
        }

        return instance;
    }

    /**
     * Save resfresh mode settings
     *
     * @param online
     * @param ctx
     */
    public void setResfrehMode(boolean online, Context ctx) {
        //save data persitent in shared prefs - to application Context
        SharedPreferences settings = ctx.getApplicationContext().getSharedPreferences(ctx.getResources().getString(R.string.settings_resfresh_mode_pref_key), ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean( ctx.getResources().getString(R.string.refresh_mode) , online);
        editor.commit();

        Log.i(TAG, "saved refresh mode: online" + online);
    }

    /**
     * Online is true, offline is false
     * @return
     */
    public boolean getRefreshMode(Context ctx) {
        SharedPreferences settings = ctx.getApplicationContext().getSharedPreferences(ctx.getResources().getString(R.string.settings_resfresh_mode_pref_key), ctx.MODE_PRIVATE);
        return settings.getBoolean(ctx.getResources().getString(R.string.refresh_mode), true); //try always load online
    }

}
