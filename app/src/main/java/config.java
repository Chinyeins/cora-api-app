package ;

public class config {
    public final static String api_host = "http://192.168.2.138/";
    public final static String api_domain = "cora/public/api/";
    public final static String api_companies = api_host+api_domain+"companies";
    public final static String api_single_company = api_host+api_domain+"company/";
    public final static String api_company_events = api_host+api_domain+"company/events/";
    public final static String api_user_competitions = api_host+api_domain+"user/competitions/";
    public final static String api_user_enlist = api_host+api_domain+"user/enlist/";
    public final static String api_user_unlist = api_host+api_domain+"user/unlist/";
    public final static String api_user_login = api_host+api_domain+"login";
}
