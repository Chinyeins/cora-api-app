package ;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import .events.DataContainer;
import .events.EventDetailFragment;
import .events.EventsFragment;
import .events.MyEventsFragment;
import .login.User;
import .login.UserController;
import .user.ProfileFragment;

public class MainActivity extends AppCompatActivity implements
        EventsFragment.OnFragmentInteractionListener,
        MyEventsFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener,
        EventDetailFragment.OnFragmentInteractionListener {

    private static final String TAG = "Maqin Activity";
    BottomNavigationView navigation;
    private EventDetailFragment eventDetailFragment = null;
    private EventsFragment eventsFragment = null;
    private ProfileFragment profileFragment = null;
    private MyEventsFragment myEventsFragment = null;
    private TextView mTextMessage;
    public Menu optionsMenu;
    public static final String eventDetailFragmentTag = "EventDetailFragment";
    public static final String myEventsFragmentTag = "MyEventsFragment";
    public static final String profileFragmentTag = "ProfileFragment";
    public static final String eventsFragmentTag = "EventsAllFragment";
    TextView headLineLabel_left;
    TextView headLineLabel_right;

    /**
     * Bottom nav
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    DataContainer.getInstance().saveState(eventsFragmentTag, getApplicationContext());
                    openEventsFragmentList();
                    return true;
                case R.id.navigation_dashboard:
                    DataContainer.getInstance().saveState(myEventsFragmentTag, getApplicationContext());
                    openMyEventsFragmentView();
                    return true;
                case R.id.navigation_notifications:
                    DataContainer.getInstance().saveState(profileFragmentTag, getApplicationContext());
                    openProfileFragment();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        headLineLabel_left = (TextView) findViewById(R.id.headline_events);
        headLineLabel_right = (TextView) findViewById(R.id.headline_details);

        if (savedInstanceState != null) {
            /**
             * Falls ein savedInstanceState vorhanden ist, in Activity Stoppen und Fragment
             * das Laden von instanceStates übernehmen lassen. Tun wir dies nicht, wird
             * im Fragment die OnCreateView methode zweimal aufgerufen!
             **/
            return;
        }
        openEventsFragmentList();
    }

    public void openEventsFragmentList() {
        final String name = getResources().getString(R.string.action_bar_title_all_events);
        FragmentManager fragmentManager = getFragmentManager();
        if (headLineLabel_left != null)
            headLineLabel_left.setText(name);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        eventsFragment = EventsFragment.newInstance(name, "");
        fragmentTransaction.replace(R.id.fragment_container_main_activity, eventsFragment, eventsFragmentTag);

        showUI(true, fragmentTransaction);
        fragmentTransaction.commit();
    }

    private void openMyEventsFragmentView() {
        final String name = getResources().getString(R.string.action_bar_title_my_events);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (headLineLabel_left != null)
            headLineLabel_left.setText(name);

        if(eventDetailFragment != null) {
            fragmentTransaction.remove(eventDetailFragment);
            eventDetailFragment = null;
        }
        myEventsFragment = MyEventsFragment.newInstance(name, "");
        fragmentTransaction.replace(R.id.fragment_container_main_activity, myEventsFragment, myEventsFragmentTag);

        showUI(true, fragmentTransaction);
        fragmentTransaction.commit();
    }

    /**
     * Single Card clicked on reclyclerview
     **/
    private void openEventDetailFragment(DataContainer.Event event) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        eventDetailFragment = EventDetailFragment.newInstance(event); // pass event data

        /*check orientation and switch to correct layout*/
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //landscape
            fragmentTransaction.replace(R.id.fragment_container_main_activity2, eventDetailFragment, eventDetailFragmentTag);
        } else {
            //portrait
            fragmentTransaction.replace(R.id.fragment_container_main_activity, eventDetailFragment, eventDetailFragmentTag);
            fragmentTransaction.addToBackStack(MainActivity.eventDetailFragmentTag);
            fragmentManager.popBackStack(profileFragmentTag, 0);
        }

        showUI(true, fragmentTransaction);

        fragmentTransaction.commit();
    }

    private void openProfileFragment() {
        final String name = "Profil";
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        profileFragment = ProfileFragment.newInstance(name, "");
        if (eventDetailFragment != null) {
            fragmentTransaction.remove(eventDetailFragment);
            eventDetailFragment = null;
        }
        if (eventsFragment != null) {
            fragmentTransaction.remove(eventsFragment);
            eventsFragment = null;
        }

        showUI(false, fragmentTransaction);
        fragmentTransaction.replace(R.id.fragment_container_profile, profileFragment, profileFragmentTag);
        fragmentTransaction.commit();
    }

    /**
     * disable / enable main layout in landscape mode
     *
     * @param b
     */
    private void showUI(boolean b, FragmentTransaction fragmentTransaction) {
        if (profileFragment != null && b) {
            fragmentTransaction.remove(profileFragment);
            profileFragment = null;
        }

        int choice = (b) ? View.VISIBLE : View.GONE;

        //hide UI
        View divider = (View) findViewById(R.id.divider2);
        View container_left = (View) findViewById(R.id.fragment_container_main_activity);
        View container_right = (View) findViewById(R.id.fragment_container_main_activity2);
        View container_profile = (View) findViewById(R.id.fragment_container_profile);

        if (container_profile != null) {
            if (b)
                container_profile.setVisibility(View.GONE);
            else
                container_profile.setVisibility(View.VISIBLE);
        }

        if (headLineLabel_left != null)
            headLineLabel_left.setVisibility(choice);

        if (headLineLabel_right != null)
            headLineLabel_right.setVisibility(choice);

        if (divider != null)
            divider.setVisibility(choice);

        if (container_left != null)
            container_left.setVisibility(choice);

        if (container_right != null)
            container_right.setVisibility(choice);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: //zurück Button
                onBackPressed();
                break;
            //TODO fix this
            /*case R.id.navigation_refresh_online:
                if(eventsFragment != null) {
                    DataContainer.getInstance().cancel(eventsFragment);
                    eventsFragment.onlineRefresh();
                    Log.d(TAG, "Refresh Presse");
                }
                break;
            case R.id.navigation_refresh_offline:
                if(eventsFragment != null) {
                    DataContainer.getInstance().cancel(eventsFragment);
                    eventsFragment.offlineRefresh();
                }
                break;*/
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_top, menu);

        MenuItem item = menu.findItem(R.id.action_layout_menu_item_switch);
        View actionView;
        SwitchCompat refreshModeSwitch = null;

        if (item != null) {
            item.setActionView(R.layout.menu_resfresh_switch_action);
            actionView = item.getActionView();
            refreshModeSwitch = (SwitchCompat) actionView.findViewById(R.id.resfresh_online_or_offline_switch);
        }

        if (refreshModeSwitch != null) {
            //register change
            final SwitchCompat finalRefreshModeSwitch = refreshModeSwitch;

            //set switch for current state
            boolean currentRefreshMode = Settings.getInstance().getRefreshMode(this);
            refreshModeSwitch.setChecked(currentRefreshMode);
            if (currentRefreshMode) {
                finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_online));
            } else {
                finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_offline));
            }

            //register listener on change
            refreshModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Settings.getInstance().setResfrehMode(isChecked, getApplicationContext());
                    finalRefreshModeSwitch.setChecked(isChecked);

                    //save changes
                    if (isChecked) {
                        finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_online));
                        User u = UserController.getInstance().getUser(getBaseContext());
                        if (u == null || !u.isLoggedIn) {
                            UserController.getInstance().logoutUser(getApplicationContext());
                            UserController.getInstance().gotoLoginActivity(getApplicationContext());
                        }
                    } else {
                        finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_offline));
                    }
                }
            });
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //load behaviour according to last know state
        switch (DataContainer.getInstance().getState(this)) {
            case eventDetailFragmentTag:
                navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
                openEventsFragmentList();
                openEventDetailFragment(null);
                break;
            case myEventsFragmentTag:
                navigation.getMenu().findItem(R.id.navigation_dashboard).setChecked(true);
                openMyEventsFragmentView();
                break;
            case profileFragmentTag:
                navigation.getMenu().findItem(R.id.navigation_notifications).setChecked(true);
                openProfileFragment();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i(TAG, "Stop activity: last state = " + DataContainer.getInstance().getState(this));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        //
    }



    @Override
    public void showOptionsMenu() {
        if (optionsMenu != null)
            onCreateOptionsMenu(optionsMenu);
    }

    @Override
    public void onBackPressed() {
        Fragment eventDetail = getFragmentManager().findFragmentByTag(eventDetailFragmentTag);
        if (eventDetail != null && eventDetail.isVisible()) {
            openEventsFragmentList();
        } else {
            // Use the Builder class for convenient dialog construction
            new AlertDialog.Builder(this)
                    .setMessage("App Beenden?")
                    .setPositiveButton("Ja, beenden", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            UserController.getInstance().resetUser(getApplication());
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).show();
            //super.onBackPressed();
        }
    }

    /**
     * Try enlist / unlist user - Anmelden Button in DetailFragment gedrückt
     *
     * @param competition
     * @param button
     */
    @Override
    public void onFragmentInteraction(DataContainer.Competition competition, View button, int eventIdFk) {
        //only En/Unlist if online
        if (competition != null && Settings.getInstance().getRefreshMode(this)) {
            if (competition.isEnlisted) {
                DataContainer.getInstance().UnlistUser(this, competition, eventDetailFragment.competitionsAdapter, eventDetailFragment.progressBar, button, eventIdFk);
            } else {
                DataContainer.getInstance().EnlistUser(this, competition, eventDetailFragment.competitionsAdapter, eventDetailFragment.progressBar, button, eventIdFk);
            }
        } else {
            Toast.makeText(this, "Sorry, du bist nicht online!", Toast.LENGTH_SHORT).show();
        }
    }

    //single event was clicked
    @Override
    public void onFragmentInteraction(DataContainer.Event event) {
        DataContainer.getInstance().saveSingleEventData(event, this);
        openEventDetailFragment(event);
        DataContainer.getInstance().saveState(eventDetailFragmentTag, getApplicationContext());
    }
}
