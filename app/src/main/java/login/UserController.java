package login;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import MainActivity;
import R;
import config;

/**
 * Handles UserData, login, register and logout
 */
public class UserController {
    private static final String TAG = "UserController";
    private static UserController instance;
    private static User _user;

    /**
     * Singleton UserController
     */
    private UserController() {
        //
    }

    /**
     * Get UserCOntroller instance
     *
     * @return
     */
    public static UserController getInstance() {
        if (instance == null) {
            instance = new UserController();
        }
        return instance;
    }

    /**
     * Load saved userdata from sharedPrefs
     * @param ctx
     * @return
     */
    public User getUser(Context ctx) {
        //load userdata from shared prefs file
        SharedPreferences userData = ctx.getSharedPreferences(ctx.getResources().getString(R.string.user_data_prefs_key), ctx.MODE_PRIVATE);
        User user = null;
        //convert back to User - From JSON
        try {
            Gson userG =  new Gson();
            String dataString = userData.getString( ctx.getResources().getString(R.string.user_pref_key), "");
            user = userG.fromJson( dataString, User.class );

            Log.i(TAG, "loaded userdata from prefs");
        } catch (Exception e) {
            Log.e(TAG, "Error parsing user data from sharedPrefs");
        }

        return user;
    }

    /**
     * Save user data to sharedPrefs
     * @param user
     * @param ctx
     */
    public void setUser(User user, Context ctx) {
        _user = user;
        if(user == null)
            return;

        String dataString = "";
        //convert obj to json
        try {
            Gson userG = new Gson();
            dataString =  userG.toJson(user);

            //save data persitent in shared prefs - to application Context
            SharedPreferences settings = ctx.getSharedPreferences(ctx.getResources().getString(R.string.user_data_prefs_key), ctx.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString( ctx.getResources().getString(R.string.user_pref_key) , dataString );

            editor.commit();

            Log.i(TAG, "saved userdata in prefs");
        } catch (Exception e) {
            Log.e(TAG, "Error converting User object to JSON String");
        }
    }

    public void resetUser(Context ctx) {
        SharedPreferences settings = ctx.getSharedPreferences(ctx.getResources().getString(R.string.user_data_prefs_key), ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(ctx.getResources().getString(R.string.user_pref_key));
        editor.commit();

        Log.i(TAG, "reset userdata in prefs");
        Toast.makeText(ctx, "Reset UserData", Toast.LENGTH_SHORT).show();
    }

    public void loginUser(final String email, final String password, final Context ctx, final ProgressBar progressBar) {
        final String url = config.api_user_login;
        final Map<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);

        RequestQueue queue = Volley.newRequestQueue(ctx);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                User user = null;

                //parse response data
                try {
                    JSONObject resp = new JSONObject(response);
                    JSONObject data = resp.getJSONObject("data");
                    JSONObject _user = data.getJSONObject("user");
                    int error = data.getInt("error");

                    if(error == 1) {
                        Toast.makeText(ctx, "Anmeldung fehlgeschlagen. Fehlerhafte Nutzerdaten", Toast.LENGTH_SHORT).show();
                        if(progressBar != null)
                            progressBar.setVisibility(View.GONE);
                        return;
                    }

                    //create user object
                    user = new User(
                            _user.getInt("id"),
                            _user.getString("name"),
                            _user.getString("email"),
                            password
                            );

                    //update user data
                    setUser(user, ctx);
                    getUser(ctx).isLoggedIn = true;

                    if(progressBar != null)
                        progressBar.setVisibility(View.GONE);

                    //goto MainActivity - only logged in users
                    Intent intent = new Intent(ctx, MainActivity.class);
                    ctx.startActivity(intent);

                } catch (Exception e) {
                    //reset user
                    resetUser(ctx);

                    if(progressBar != null)
                        progressBar.setVisibility(View.GONE);

                    Toast.makeText(ctx, "Fehler beim Anmelden. Bitte versuche es erneut", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Konnte User Informationen nich parsen. JSON PARSE ERROR");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //reset user
                resetUser(ctx);

                if(progressBar != null)
                    progressBar.setVisibility(View.GONE);

                Toast.makeText(ctx, "Anmeldung fehlgeschlagen. Bitte versuche es erneut", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        //make request
        queue.add(stringRequest);
    }

    public boolean IsLoggedIn(Context ctx) {
        return getUser(ctx).isLoggedIn;
    }

    public boolean logoutUser(Context ctx) {
        resetUser(ctx);
        return true;
    }

    public void gotoLoginActivity(Context ctx) {
        ctx.startActivity(new Intent(ctx, LoginActivity.class));
    }
}

