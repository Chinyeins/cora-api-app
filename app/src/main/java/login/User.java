package login;

import java.io.Serializable;


/**
 * User Model
 */
public class User implements Serializable {
    public int id;
    public String name;
    public String email;
    public boolean isLoggedIn;
    public String password;

    User (final int id, String name, String email, final String password){
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.isLoggedIn = false;
    }
}
