 de.tphi.fh_kiel.mob1819.sp.mob1819_sp_926707_926608.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import de.tphi.fh_kiel.mob1819.sp.mob1819_sp_926707_926608.MainActivity;
import de.tphi.fh_kiel.mob1819.sp.mob1819_sp_926707_926608.R;
import de.tphi.fh_kiel.mob1819.sp.mob1819_sp_926707_926608.Settings;
import de.tphi.fh_kiel.mob1819.sp.mob1819_sp_926707_926608.events.DataContainer;

public class LoginActivity extends AppCompatActivity {
    EditText _email;
    EditText _password;
    Button loginBtn;
    ProgressBar progressBar;
    public Menu optionsMenu;
    public boolean loggedOut = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //check if user has intentionally logged out
        Intent intent = getIntent();
        loggedOut = intent.getBooleanExtra("hasLoggedOut", false);

        _email = (EditText) findViewById(R.id.login_input_email);
        _password = (EditText) findViewById(R.id.login_password_input);
        loginBtn = (Button) findViewById(R.id.login_login_btn);
        progressBar = (ProgressBar) findViewById(R.id.loginProgressBar);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Settings.getInstance().getRefreshMode(getApplicationContext())) {
                    LoginOffline();
                    return;
                }

                //try login
                if (_email.getText().toString().equals("") || _password.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Bitte Prüfen Sie Ihre Eingabe", Toast.LENGTH_SHORT).show();
                } else {
                    if (progressBar != null)
                        progressBar.setVisibility(View.VISIBLE);

                    UserController.getInstance().loginUser(_email.getText().toString(), _password.getText().toString(), getApplicationContext(), progressBar);
                    //disable keyboard
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(LoginActivity.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                        //
                    }
                }
            }
        });

        //try login with saved data - only if user hasn´t logged out before
        if(!loggedOut)
            loadUserData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_top, menu);

        MenuItem item = menu.findItem(R.id.action_layout_menu_item_switch);
        View actionView;
        SwitchCompat refreshModeSwitch = null;

        if (item != null) {
            item.setActionView(R.layout.menu_resfresh_switch_action);
            actionView = item.getActionView();
            refreshModeSwitch = (SwitchCompat) actionView.findViewById(R.id.resfresh_online_or_offline_switch);
        }

        if (refreshModeSwitch != null) {
            //register change
            final SwitchCompat finalRefreshModeSwitch = refreshModeSwitch;

            //set switch for current state
            boolean currentRefreshMode = Settings.getInstance().getRefreshMode(this);
            refreshModeSwitch.setChecked(currentRefreshMode);
            if (currentRefreshMode) {
                finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_online));
            } else {
                finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_offline));
            }

            //register listener on change
            refreshModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Settings.getInstance().setResfrehMode(isChecked, getApplicationContext());
                    finalRefreshModeSwitch.setChecked(isChecked);

                    //save changes
                    if (isChecked) {
                        finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_online));
                    } else {
                        finalRefreshModeSwitch.setText(getResources().getString(R.string.refresh_offline));
                    }
                }
            });
        }

        return true;
    }

    public void loadUserData() {
        if (!Settings.getInstance().getRefreshMode(getApplicationContext())) {
            LoginOffline();
            return;
        }

        User user = UserController.getInstance().getUser(getApplicationContext());
        if (user != null) {
            if (user.email == null || user.password == null) {
                progressBar.setVisibility(View.VISIBLE);
                //reset user
                UserController.getInstance().resetUser(this);
                progressBar.setVisibility(View.GONE);
            } else {
                //attempt login with stored credentials
                progressBar.setVisibility(View.VISIBLE);
                _email.setText(user.email);
                _password.setText(user.password);
                UserController.getInstance().loginUser(user.email, user.password, getApplicationContext(), progressBar);
            }
        }
    }

    public void LoginOffline() {
        //enter as online
        Intent inten = new Intent(this, MainActivity.class);
        startActivity(inten);
    }
}
