package events;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.List;

import MainActivity;
import R;
import Settings;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EventDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EventDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventDetailFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_EVENT = "event";
    static final String TAG = "EventDetailFragment";
    private String mParam1;
    private String mParam2;

    private DataContainer.Event event;
    public SwipeRefreshLayout progressBar;

    private ImageView eventImageView;
    private OnFragmentInteractionListener mListener;

    public CompetitionsAdapter competitionsAdapter;

    public EventDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment EventDetailFragment.
     */
    public static EventDetailFragment newInstance(DataContainer.Event event) {
        EventDetailFragment fragment = new EventDetailFragment();
        Bundle args = new Bundle();
        if(event != null) {
            args.putSerializable(ARG_EVENT, event); // put event data in bundle
            args.putString(ARG_PARAM1, event.name);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get event data
        Bundle _bundle = getArguments();
        if (_bundle != null) {
            mParam1 = _bundle.getString(ARG_PARAM1);
            event = (DataContainer.Event) _bundle.getSerializable(ARG_EVENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_detail, container, false);
        progressBar = view.findViewById(R.id.progressBar);
        eventImageView = view.findViewById(R.id.detailImageView);

        //setup RecylcerView
        setupRecycleView(view, mListener);
        //load event image
        loadImageAsync();

        //register swipe to refresh
        progressBar.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(getContext());
            }
        });

        //maybe pop EventDetailFragment from Stack - if in Portrait Mode
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            try {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.action_bar_title_all_events));
            } catch (Exception e) {
                Log.w("getSupportActionBar", "Couldn´t getSupportActionBar()...");
            }
        }

        return view;
    }

    /**
     * load images with picasso
     */
    void loadImageAsync() {
        if (event != null && event.img_src != null && !event.img_src.equals("null")) {
            Picasso.get()
                    .load(event.img_src)
                    .resize(0, 256)
                    .into(eventImageView);
        }
    }

    /**
     *
     * @param view
     */
    public void setupRecycleView(View view, OnFragmentInteractionListener mListener) {
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.competitionsRecyclerView);

        //check for orientation
        if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LinearLayoutManager llm = new GridLayoutManager(getContext(), 1);
            rv.setLayoutManager(llm);
        } else {
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            rv.setLayoutManager(llm);
        }
        List<DataContainer.Competition> competitions = null;
        if(event !=  null && event.competitions != null) {
            competitions = event.competitions;
        }
        competitionsAdapter = new CompetitionsAdapter(competitions, mListener);
        rv.setAdapter(competitionsAdapter);
    }

    /**
     * Load Events
     */
    void loadData(Context ctx) {
        if (Settings.getInstance().getRefreshMode(getActivity().getApplicationContext())) {
            //load online
            onlineRefresh(ctx);
            Toast.makeText(getActivity().getApplicationContext(), "Lade Online Daten", Toast.LENGTH_SHORT).show();
        } else {
            //load offline
            offlineRefresh(ctx);
            Toast.makeText(getActivity().getApplicationContext(), "Lade Offline Daten", Toast.LENGTH_SHORT).show();
        }

        //update event image
        loadImageAsync();
    }

    public void onlineRefresh(Context ctx) {
        if(event == null) //get last selected event
            this.event = DataContainer.getInstance().getSingleEventData(ctx);

        DataContainer.getInstance().updateCompetitionsAdapterOnline(event, getContext(), competitionsAdapter, this, progressBar);
    }

    public void offlineRefresh(Context ctx) {
        if (competitionsAdapter != null) {
            //load event
            //this.event = DataContainer.getInstance().getSingleEventData(getContext());
            //load competitions
            //this.competitionsAdapter.updateData( DataContainer.getInstance().getCompetitionsData(getContext()) );

            if(progressBar != null)
                progressBar.setRefreshing(false);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if(getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE){
            try {
                ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(event.name);
            } catch (Exception e) {
                Log.w("getSupportActionBar", "Couldn´t getSupportActionBar()...");
            }
        }

        //load data
        loadData(getContext());
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        DataContainer.getInstance().cancel(this);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(DataContainer.Competition singleCompetition, View button, int eventIdFk);
    }

}
