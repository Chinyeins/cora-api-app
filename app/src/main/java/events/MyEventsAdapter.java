package .events;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import .R;

public class MyEventsAdapter extends RecyclerView.Adapter<MyEventsAdapter.MyEventViewHolder> {
    private static final String TAG = "MyEventsAdapter";
    //vars
    List<DataContainer.Competition> competitions;
    Context fragmentContext;
    private MyEventsFragment.OnFragmentInteractionListener mListener;
    Object tag;

    /**
     * View Holder
     */
    public static class MyEventViewHolder extends RecyclerView.ViewHolder {
        TextView competitionName;
        TextView descTextView;
        TextView dateTextView;
        TextView slotsTextView;
        Button enlistBtn;

        @SuppressLint("ResourceType")
        MyEventViewHolder(View itemView) {
            super(itemView);
            competitionName = (TextView) itemView.findViewById(R.id.competitionNameTextView);
            descTextView = (TextView) itemView.findViewById(R.id.descTextView);
            dateTextView = (TextView) itemView.findViewById(R.id.dateTextView);
            slotsTextView = (TextView) itemView.findViewById(R.id.slotsTextView);
            enlistBtn = (Button) itemView.findViewById(R.id.registerButton);
        }
    }

    /**
     * Constructor
     * @param competitions
     * @param mListener
     * @param fragmentContext
     * @param tag
     */
    MyEventsAdapter(List<DataContainer.Competition> competitions, MyEventsFragment.OnFragmentInteractionListener mListener, Context fragmentContext, Object tag){
        this.competitions = competitions;
        this.mListener = mListener;
        this.fragmentContext = fragmentContext;
        this.tag = tag;
    }

    @NonNull
    @Override
    public MyEventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.competitions_card_view, parent, false);;
        MyEventViewHolder evh = new MyEventViewHolder(v);
        return evh;
    }


    @Override
    public void onBindViewHolder(@NonNull MyEventViewHolder holder, final int position) {
        DataContainer.Competition competition = competitions.get(position);

        //bind data
        holder.competitionName.setText(competition.name);
        if(competition.description.equals(""))
            holder.descTextView.setText("Keine Beschreibung vorhanden");
        else
            holder.descTextView.setText(competition.description);

        holder.slotsTextView.setText("Anmeldungen: "+competitions.get(position).slots_used+"/"+competitions.get(position).slots_max);
        holder.dateTextView.setText("Beginn:"+  (competition.start.toLocaleString()).substring(0, 10) );
        holder.enlistBtn.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    DataContainer.getInstance().openSingleEventOnline(competitions.get(position).eventIdFk, fragmentContext, mListener, tag);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        if(competitions != null)
            return competitions.size();
        else
            return 0;
    }

    public void updateData(List<DataContainer.Competition> competitions, Context ctx) {
        this.competitions = competitions;
        notifyDataSetChanged();

        //get no data found label
        View _view = ((Activity)ctx).findViewById(R.id.no_data_found_label);
        if(_view != null) {
            //check item count
            if(getItemCount() <= 0) {
                // no data found show
                _view.setVisibility(View.VISIBLE);
            } else {
                // no data found hide
                _view.setVisibility(View.GONE);
            }
        }

        Log.e(TAG, "update competitions from my Events Adapter"+competitions.size());
    }


}
