

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



public class CompetitionsAdapter extends RecyclerView.Adapter<CompetitionsAdapter.CompetitionsViewHolder> {
    private static final String TAG = "CompetitionsAdapter";
    //vars
    List<DataContainer.Competition> competitions;
    private EventDetailFragment.OnFragmentInteractionListener mListener;
    private CompetitionsAdapter competitionsAdapter;

    /**
     * View Holder
     */
    public static class CompetitionsViewHolder extends RecyclerView.ViewHolder {
        TextView competitionName;
        TextView descTextView;
        TextView dateTextView;
        TextView slotsTextView;
        Button enlistBtn;

        public CompetitionsViewHolder(View itemView) {
            super(itemView);
            competitionName = (TextView) itemView.findViewById(R.id.competitionNameTextView);
            descTextView = (TextView) itemView.findViewById(R.id.descTextView);
            dateTextView = (TextView) itemView.findViewById(R.id.dateTextView);
            slotsTextView = (TextView) itemView.findViewById(R.id.slotsTextView);
            enlistBtn = (Button) itemView.findViewById(R.id.registerButton);
        }
    }

    /**
     * Constructor
     * @param competitions
     * @param mListener
     */
    CompetitionsAdapter(List<DataContainer.Competition> competitions, EventDetailFragment.OnFragmentInteractionListener mListener){
        this.competitions = competitions;
        this.mListener = mListener;
        this.competitionsAdapter = this;
    }


    @NonNull
    @Override
    public CompetitionsAdapter.CompetitionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.competitions_card_view, parent, false);
        CompetitionsViewHolder cvh = new CompetitionsViewHolder(v);
        return cvh;
    }


    @Override
    public void onBindViewHolder(@NonNull CompetitionsAdapter.CompetitionsViewHolder holder, final int position) {
        DataContainer.Competition competition = competitions.get(position);

        //bind data
        holder.competitionName.setText(competition.name);
        if(competition.description.equals(""))
            holder.descTextView.setText("Keine Beschreibung vorhanden");
        else
            holder.descTextView.setText(competition.description);

        holder.slotsTextView.setText("Anmeldungen: "+competitions.get(position).slots_used+"/"+competitions.get(position).slots_max);
        if (competition.start != null)
            holder.dateTextView.setText("Beginn:"+  (competition.start.toLocaleString()).substring(0, 10) );
        holder.enlistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null)
                    mListener.onFragmentInteraction(competitions.get(position), v, competitions.get(position).eventIdFk);
            }
        });

        //set Button according to whether a user is already enlisted to a competition or not
        if(competition.isEnlisted) {
            //show enList style
            holder.enlistBtn.setText( holder.itemView.getContext().getResources().getString(R.string.btn_un_list_text) );
            holder.enlistBtn.setBackgroundColor(Color.RED);
        } else {
            //show unList style
            holder.enlistBtn.setText( holder.itemView.getContext().getResources().getString(R.string.btn_enlist_text) );
            holder.enlistBtn.setBackgroundColor(Color.GREEN);
        }

    }



    @Override
    public int getItemCount() {
        if(competitions != null)
            return competitions.size();
        else {
            return 0;
        }
    }

    /**
     * Update Adapter
     * @param competitions
     */
    public void updateData(List<DataContainer.Competition> competitions) {
        this.competitions = competitions;
        notifyDataSetChanged();
        Log.e(TAG, "update competitions"+competitions.size());
    }
}
