package .events;

import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.ContactsContract;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import .MainActivity;
import .R;
import .Settings;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EventsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EventsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "EventsFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private EventsAdapter eventsAdapter;
    private SwipeRefreshLayout progressBar;
    private ProgressBar offlineProgressBar;

    public EventsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EventsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EventsFragment newInstance(String param1, String param2) {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        List<DataContainer.Event> _events = null;
        if(eventsAdapter != null)
            _events = eventsAdapter.events;
        try {
            String dataString = new Gson().toJson(_events);
            outState.putString(getString(R.string.saved_state_events), dataString);
        } catch (Exception e) {
            //
        }

        Log.v(TAG, "save events for IPC");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        progressBar = view.findViewById(R.id.eventsProgressBar);
        offlineProgressBar = view.findViewById(R.id.offlineProgressBar);

        //init data
        setupRecycleView(view, mListener);

        //setup swipe refresh listener
        if (progressBar != null) {
            progressBar.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadData(getContext());
                }
            });
        }

        //check if state can be restored - change in orientation etc.
        if(savedInstanceState != null) {
            //restore state - load events data
            List<DataContainer.Event> _events = restoreSavedEvents(savedInstanceState);
            eventsAdapter.updateData(_events);
        } else {
            //load data - no state to restore
            if (!DataContainer.getInstance().isLoaded())
                loadData(getContext());
        }
        return view;
    }

    /**
     * Lade Event Daten aus in Bundle gespeicherten Daten (IPC)
     * @param savedInstanceState
     * @return
     */
    private List<DataContainer.Event> restoreSavedEvents(Bundle savedInstanceState) {
        String dataString = savedInstanceState.getString( getString(R.string.saved_state_events) );
        List<DataContainer.Event> _events = new ArrayList<>();
        try {
            JSONArray data = new JSONArray(dataString);
            for(int i = 0; i < data.length(); i ++) {
                _events.add(
                        new Gson().fromJson(
                                data.getJSONObject(i).toString(), DataContainer.Event.class ));
            }
        } catch(Exception e) {
            Log.e(TAG, "error parsing saved state events");
        }

        //restore events
        Toast.makeText(getContext(), "Lade events IPC", Toast.LENGTH_SHORT).show();
        return _events;
    }

    /**
     * Intialize RecyclerView with Dummy Data ()
     *
     * @param view
     * @param mListener
     */
    public void setupRecycleView(View view, OnFragmentInteractionListener mListener) {
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.entdeckenRecyclerView);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            LinearLayoutManager llm = new GridLayoutManager(getContext(), 2);
            rv.setLayoutManager(llm);
        } else {
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            rv.setLayoutManager(llm);
        }
        /*In contrast to other adapter-backed views such as ListView
         * or GridView, RecyclerView allows client code to provide custom
         * layout arrangements for child views. These arrangements are controlled by the
         * LayoutManager. A LayoutManager must be provided for RecyclerView to function.
         **/

        //The Adapter handles the view data binding
        eventsAdapter = new EventsAdapter(DataContainer.getInstance().getEventsData(), mListener);
        rv.setAdapter(eventsAdapter);
    }

    /**
     * Load Events
     */
    void loadData(Context ctx) {
        if (Settings.getInstance().getRefreshMode(getActivity().getApplicationContext())) {
            //load online
            onlineRefresh(ctx);
            Toast.makeText(getActivity().getApplicationContext(), "Lade Online Daten", Toast.LENGTH_SHORT).show();
        } else {
            //load offline
            offlineRefresh(ctx);
            Toast.makeText(getActivity().getApplicationContext(), "Lade Offline Daten", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Save Events
     *
     * @param events
     * @param ctx
     */
    void saveData(List<DataContainer.Event> events, Context ctx) {
        try {
            SharedPreferences eventsData = ctx.getSharedPreferences(ctx.getResources().getString(R.string.events_data_prefs_key), ctx.MODE_PRIVATE);
            SharedPreferences.Editor editor = eventsData.edit();
            String dataString = new Gson().toJson(events);
            editor.putString(ctx.getResources().getString(R.string.events_key), dataString);
            editor.apply();
            Log.i(TAG, "Saved events Data");
        } catch (Exception e) {
             //may produce runtime error - OS is then unable to stop activity
        }
    }

    public void onlineRefresh(Context ctx) {
        if (eventsAdapter != null && ctx != null)
            DataContainer.getInstance().updateEventsAdapterOnline(getContext(), eventsAdapter, this, progressBar);
    }

    public void offlineRefresh(Context ctx) {
        if (eventsAdapter != null) {
            SharedPreferences eventsData = ctx.getSharedPreferences(ctx.getResources().getString(R.string.events_data_prefs_key), ctx.MODE_PRIVATE);
            List<DataContainer.Event> _events;
            try {
                String dataString = eventsData.getString(ctx.getResources().getString(R.string.events_key), "");
                JSONArray _data = new JSONArray(dataString);
            } catch (Exception e) {
                //
            }
            DataContainer.getInstance().updateEventsAdapterOffline(eventsAdapter, progressBar, offlineProgressBar);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.action_bar_title_all_events));
        } catch (Exception e) {
            Log.w("getSupportActionBar", "Couldn´t getSupportActionBar()...");
        }

        //initially load data
        if (!DataContainer.getInstance().isLoaded()) {
            loadData(getContext());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //load prefs
        if (eventsAdapter != null) {
            saveData(eventsAdapter.events, getContext());
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DataContainer.getInstance().cancel(this);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(DataContainer.Event event);
        void showOptionsMenu();
    }
}
