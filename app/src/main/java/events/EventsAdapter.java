package .events;

import android.annotation.SuppressLint;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import .R;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder>{
    private static final String TAG = "EventsAdapter";
    List<DataContainer.Event> events;
    private EventsFragment.OnFragmentInteractionListener mListener;

    /**
     * Constructor
     */
    EventsAdapter(List<DataContainer.Event> events, EventsFragment.OnFragmentInteractionListener mListener)
    {
        this.events = events;
        this.mListener = mListener;
    }

    /**
     * View Holder of Adapter
     */
    public static class EventViewHolder extends RecyclerView.ViewHolder {
        TextView eventTitle;
        ImageView imageView;

        @SuppressLint("ResourceType")
        EventViewHolder(View itemView) {
            super(itemView);
            eventTitle = (TextView) itemView.findViewById(R.id.eventTitleTextView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
        }

        public void cleanup() {
            Picasso.get()
            .cancelRequest(imageView);
        }
    }

    /**
     * Inflate ViewHolder (CardView Events All)
     * @param parent
     * @param viewType
     * @return EventViewHolder
     */
    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.events_all_card_view, parent, false);
        EventViewHolder evh = new EventViewHolder(v);
        return evh;
    }

    /**
     * Actually write data to single CardsViews here
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, final int position) {
        //set cardView elements
        holder.eventTitle.setText(events.get(position).name);

        if (events.get(position).img_src != null && !events.get(position).img_src.equals("null")) {
            Picasso.get()
                    .load(events.get(position).img_src)
                    .resize(384, 184)
                    .into(holder.imageView);
        }

        //set item clicked listener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onFragmentInteraction(events.get(position));
                }
                else
                    Log.i("TEST", events.get(position).name);
            }
        });
    }

    @Override
    public void onViewRecycled(@NonNull EventViewHolder holder) {
        holder.cleanup();
    }

    @Override
    public int getItemCount() {
        if(events != null)
            return events.size();
        else {
            return 0;
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void updateData(List<DataContainer.Event> events) {
        this.events = events;
        notifyDataSetChanged();
        Log.i(TAG, "updated events: size=" + events.size());
    }
}
