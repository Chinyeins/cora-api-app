package .events;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.ContactsContract;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Constraints;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.List;

import .R;
import .events.DataContainer.Event;
import .Settings;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyEventsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyEventsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyEventsFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "MyEventsFragment";
    private String mParam1;
    private MyEventsAdapter myEventsAdapter;
    List<DataContainer.Competition> competitions = null;
    private OnFragmentInteractionListener mListener;
    public SwipeRefreshLayout progressBar;

    public MyEventsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyEventsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyEventsFragment newInstance(String param1, String param2) {
        MyEventsFragment fragment = new MyEventsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_events, container, false);
        progressBar = view.findViewById(R.id.myEventsProgressBar);
        //setup recyclerView
        setupRecycleView(view, mListener);
        //register swipe
        progressBar.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(getContext());
            }
        });
        return view;
    }

    /**
     * @param view
     * @param mListener
     */
    public void setupRecycleView(View view, OnFragmentInteractionListener mListener) {
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.myEventsRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(llm);

        myEventsAdapter = new MyEventsAdapter(null, mListener, getContext(), null);

        rv.setAdapter(myEventsAdapter);
    }


    /**
     * Load Events
     */
    void loadData(Context ctx) {
        if (Settings.getInstance().getRefreshMode(getActivity().getApplicationContext())) {
            //load online
            onlineRefresh(ctx);
            Toast.makeText(getActivity().getApplicationContext(), "Lade Online Daten", Toast.LENGTH_SHORT).show();
        } else {
            //load offline
            offlineRefresh(ctx);
            Toast.makeText(getActivity().getApplicationContext(), "Sie sind offline. Bitte gehen Sie online, um diesen Inhalt anzuzeigen", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Save Events
     *
     * @param events
     * @param ctx
     */
    void saveData(List<DataContainer.Competition> events, Context ctx) {
        SharedPreferences eventsData = ctx.getSharedPreferences(ctx.getResources().getString(R.string.events_data_prefs_key), ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = eventsData.edit();

        String dataString = new Gson().toJson(events);
        editor.putString(ctx.getResources().getString(R.string.events_key), dataString);
        editor.apply();

        Log.i(TAG, "Saved events Data");
    }

    public void onlineRefresh(Context ctx) {
        if (myEventsAdapter != null && ctx != null) {
            DataContainer.getInstance().updateMyEventsAdapterOnline(ctx, myEventsAdapter, progressBar);
        }
    }

    public void offlineRefresh(Context ctx) {
        if (myEventsAdapter != null) {
            SharedPreferences myEventsData = ctx.getSharedPreferences(ctx.getResources().getString(R.string.competition_data_prefs_key), ctx.MODE_PRIVATE);
            List<DataContainer.Event> _events;
            try {
                String dataString = myEventsData.getString(ctx.getResources().getString(R.string.my_events_competition_key), "");
                JSONArray _data = new JSONArray(dataString);

            } catch (Exception e) {
                //
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //init data
        loadData(getContext());
    }

    @Override
    public void onStop() {
        super.onStop();

        //init data
        saveData(competitions, getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Meine Anmeldungen");
        } catch (Exception e) {
            Log.w("getSupportActionBar", "Couldn´t getSupportActionBar()...");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DataContainer.getInstance().cancel(this);
    }

    /**
     * Try enlist user to course
     */
    public void EnlistUser() {

    }

    /**
     * Try remove user from course
     */
    public void UnlistUser() {

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(DataContainer.Event event);
    }
}
