package .events;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import .R;
import .dummy.CompetitionsContent;
import .login.User;
import .login.UserController;
import .config;
/**
 * Creates fake data for testing the ui
 */
public class DataContainer implements Serializable {
    private static DataContainer dataContainerInstance;
    private static String TAG = "DataContainer";
    private RequestQueue requestQueue;
    private AsyncFillEventsAdapter asyncFillEventsAdapter;
    private List<Event> events;
    private boolean loaded;
    //private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private List<Competition> myCompetitions = new ArrayList<>();


    /**
     * Events = Veranstaltungen. Enthalten Competitions (Wettbewerbe)
     */
    public class Event implements Serializable {
        public int id;
        public String name;
        public String img_src;
        public List<Competition> competitions;

        Event(int id, String name, List<Competition> competitions) {
            this.id = id;
            this.name = name;
            this.competitions = competitions;
        }

        Event(int id, String name, String img_src, List<Competition> competitions) {
            this.id = id;
            this.name = name;
            this.img_src = img_src;
            this.competitions = competitions;
        }
    }

    /**
     * Wettbewerbe
     */
    public class Competition implements Serializable {
        public int id;
        public String name;
        public int category_id;
        public int status;
        public Date start;
        public Date end;
        public int slots_max;
        public int slots_used;
        public String description;
        public int eventIdFk;
        public boolean isEnlisted;

        Competition(int id, String name, int category_id, int status, Date start, Date end, int slots_max, int slots_used, String description) {
            this.id = id;
            this.name = name;
            this.category_id = category_id;
            this.status = status;
            this.start = start;
            this.end = end;
            this.slots_max = slots_max;
            this.slots_used = slots_used;
            this.description = description;
        }

        Competition(int id, String name, int category_id, int status, Date start, Date end, int slots_max, int slots_used, String description, int eventIdFk, boolean isEnlisted) {
            this.id = id;
            this.name = name;
            this.category_id = category_id;
            this.status = status;
            this.start = start;
            this.end = end;
            this.slots_max = slots_max;
            this.slots_used = slots_used;
            this.description = description;
            this.eventIdFk = eventIdFk;
            this.isEnlisted = isEnlisted;
        }

        Competition(int id, String name, int category_id, int status, Date start, Date end, int slots_max, int slots_used, String description, int eventIdFk) {
            this.id = id;
            this.name = name;
            this.category_id = category_id;
            this.status = status;
            this.start = start;
            this.end = end;
            this.slots_max = slots_max;
            this.slots_used = slots_used;
            this.description = description;
            this.eventIdFk = eventIdFk;
        }
    }

    /**
     * Create new event from inner Class Event class
     *
     * @param id
     * @param name
     * @param competitions
     * @return
     */
    public DataContainer.Event newEvent(final int id, final String name, List<Competition> competitions) {
        return new Event(id, name, competitions);
    }

    public Event newEvent(final int id, final String name, final String imgSrc, List<Competition> competitions) {
        return new Event(id, name, competitions);
    }

    /**
     * std. Constructor
     */
    private DataContainer() {
        loaded = false;
        events = new ArrayList<>();
    }

    /**
     * Singelton
     *
     * @return DataContainer
     */
    public static DataContainer getInstance() {
        if (dataContainerInstance == null) {
            synchronized (DataContainer.class) {
                if (dataContainerInstance == null) {
                    dataContainerInstance = new DataContainer();
                }
            }
        }
        return dataContainerInstance;
    }

    private RequestQueue getRequestQueue(Context context) {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    /**
     * Get Event Data
     *
     * @return events
     */
    public List<Event> getEventsData() {
        return events;
    }

    public List<Competition> getMyCompetitions() {
        return myCompetitions;
    }

    public boolean isLoaded() {
        return loaded;
    }

    /**
     * Update Events from offline data
     *
     * @param eventsAdapter
     * @param progressBar
     */
    public void updateEventsAdapterOffline(EventsAdapter eventsAdapter, SwipeRefreshLayout progressBar, ProgressBar offlineProgressBar) {

        if (asyncFillEventsAdapter == null) {
            asyncFillEventsAdapter = new AsyncFillEventsAdapter();
            asyncFillEventsAdapter.setProgressBar(progressBar, offlineProgressBar);
            asyncFillEventsAdapter.execute();
        }

        asyncFillEventsAdapter.setEventsAdapter(eventsAdapter);
    }


    /**
     * ASync TASK:
     */
    private class AsyncFillEventsAdapter extends AsyncTask<Event, Integer, Event> {

        EventsAdapter eventsAdapter;
        SwipeRefreshLayout progressBar;
        ProgressBar offlineProgressBar;

        public void setEventsAdapter(EventsAdapter eventsAdapter) {
            this.eventsAdapter = eventsAdapter;
        }

        public void setProgressBar(SwipeRefreshLayout progressBar, ProgressBar offlineProgressBar) {
            this.progressBar = progressBar;
            this.offlineProgressBar = offlineProgressBar;
        }

        /**
         * Ausführung auf MainThread (UI)
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loaded = false;
            events = new ArrayList<>();
            if (progressBar != null)
                progressBar.setRefreshing(true);
            if (offlineProgressBar != null) {
                offlineProgressBar.setVisibility(View.VISIBLE);
                offlineProgressBar.setProgress(5); //reset progress
            }

        }


        /**
         * Führe Aufgabe im Hintergrund aus (Eigener Thread!)
         * @param events1
         * @return
         */
        @Override
        protected Event doInBackground(Event... events1) {
            /**
            * Generiere Dummy Daten
            */
            //Wettbewerbe dummy Daten
            List<Competition> competitions1 = new ArrayList<>();
            competitions1.add(new Competition(1, "Mathemagier", 1, 0, null, null, 100, 21, "Werde der beste Mathemagier der Welt", 0, false));
            competitions1.add(new Competition(2, "Infomagier", 1, 0, null, null, 150, 94, "Werde der beste Infomagier der Welt", 0, false));
            competitions1.add(new Competition(3, "Mediamagier", 2, 0, null, null, 80, 80, "Werde der beste Mediamagier der Welt", 0, false));
            competitions1.add(new Competition(4, "Elektromagier", 1, 0, null, null, 50, 9, "Werde der beste Elektromagier der Welt", 0, false));

            List<Competition> competitions2 = new ArrayList<>();
            competitions2.add(new Competition(5, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe", 1, false));
            competitions2.add(new Competition(6, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",1, false));
            competitions2.add(new Competition(7, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",1, false));

            List<Competition> competitions3 = new ArrayList<>();
            competitions3.add(new Competition(8, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",2, false));
            competitions3.add(new Competition(9, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",2, false));

            List<Competition> competitions4 = new ArrayList<>();
            competitions4.add(new Competition(10, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",3, false));

            List<Competition> competitions5 = new ArrayList<>();
            competitions5.add(new Competition(11, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(12, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(13, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(14, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(15, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(16, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(17, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(18, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(19, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));
            competitions5.add(new Competition(20, "Dummywettbewerb", 3, 0, null, null, 10, 6, "Löse am schnellsten die gestellte Aufgabe",5, false));

            //Events
            events.add(new Event(0, "Fachhochschule Kiel", competitions1));
            SystemClock.sleep(200);
            publishProgress(1);
            events.add(new Event(1, "Dummyveranstaltung 1", competitions2));
            SystemClock.sleep(200);
            publishProgress(2);
            events.add(new Event(2, "Dummyveranstaltung 2", competitions3));
            SystemClock.sleep(200);
            publishProgress(3);
            events.add(new Event(3, "Dummyveranstaltung 3", competitions4));
            SystemClock.sleep(200);
            publishProgress(4);
            events.add(new Event(4, "Dummyveranstaltung 4", competitions5));
            SystemClock.sleep(200);

            /**
             * Sende Informationen zurück an onProgressUpdate
             */
            publishProgress(5);

            for (int i = 5; i < 100; i++) {
                if (!isCancelled()) {
                    events.add(new Event(10 + i, "Dummyveranstaltung " + i, competitions5));
                    SystemClock.sleep(200);
                    publishProgress(i);
                } else {
                    publishProgress(i);
                    return null;
                }
            }

            return null;
        }


        /***
         * Wird im UI Thread ausgeführt -> Befüllen der ProgressBar
         *
         * @param progress
         */
        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            if (eventsAdapter != null)
                eventsAdapter.updateData(events);
            if (isCancelled() && progressBar != null)
                progressBar.setRefreshing(false);

            if (offlineProgressBar != null) {
                //update progressBar
                offlineProgressBar.setProgress(progress[0]);
                if (isCancelled())
                    offlineProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onPostExecute(Event event) {
            super.onPostExecute(event);
            Log.i(TAG, "post execute asynctask events size: " + events.size());
            if (eventsAdapter != null)
                eventsAdapter.updateData(events);
            Log.i(TAG, "post execute asynctask events size: " + events.size());
            loaded = true;
            if (progressBar != null)
                progressBar.setRefreshing(false);
            if (offlineProgressBar != null) {
                offlineProgressBar.setVisibility(View.GONE);
                Toast.makeText(offlineProgressBar.getContext(), "Daten erfolgreich geladen", Toast.LENGTH_SHORT).show();
            }

            asyncFillEventsAdapter = null;

            Log.i(TAG, "post execute asynctask DONE --> FULLY LOADED" );

        }
    }

    public void updateEventsAdapterOnline(final Context context, final EventsAdapter eventsAdapter, Object tag, final SwipeRefreshLayout progressBar) {

        loaded = false;
        String url = config.api_companies;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        events = new ArrayList<>();

                        //parse response json string
                        try {
                            //get all events from json string
                            JSONObject data = new JSONObject(response);
                            JSONArray jsonEvents = data.getJSONArray("data");

                            //convert response data to arrayList events
                            for (int i = 0; i < jsonEvents.length(); i++) {
                                //get event from json
                                JSONObject event = jsonEvents.getJSONObject(i);

                                //convert event to custom data structure event
                                events.add(new Event(
                                        event.getInt("id"),
                                        event.getString("co_name"),
                                        event.getString("co_img_src"),
                                        null
                                ));
                            }

                            //update data
                            eventsAdapter.updateData(events);
                            loaded = true;

                            Toast.makeText(context, "Geladen", Toast.LENGTH_LONG).show();
                            //disable spinner
                            if (progressBar != null) {
                                progressBar.setRefreshing(false);
                            }

                        } catch (Exception e) {
                            //
                            Log.e(TAG, "ERROR parsing events JSON");
                            Toast.makeText(context, "Fehler", Toast.LENGTH_LONG).show();
                            //disable spinner
                            if (progressBar != null) {

                                progressBar.setRefreshing(false);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Volley" + error.getMessage());
                Toast.makeText(context, "Fehler", Toast.LENGTH_LONG).show();
                //disable spinner
                if (progressBar != null) {
                    progressBar.setRefreshing(false);
                }
            }
        });

        if (progressBar != null)
            progressBar.setRefreshing(true);

        stringRequest.setTag(tag);
        // Add the request to the RequestQueue.
        getRequestQueue(context).add(stringRequest);
    }

    public void openSingleEventOnline(final int eventId, final Context context, final MyEventsFragment.OnFragmentInteractionListener mListener, Object tag) {

        String url = config.api_single_company + eventId;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //parse response json string
                        try {
                            //get event from json string
                            JSONObject data = new JSONObject(response);
                            JSONObject event = data.getJSONObject("data");

                            //convert event to custom data structure event
                            Event singleEvent = new Event(
                                    event.getInt("id"),
                                    event.getString("co_name"),
                                    event.getString("co_img_src"),
                                    null
                            );

                            mListener.onFragmentInteraction(singleEvent);

                        } catch (Exception e) {
                            //
                            Log.e(TAG, "ERROR parsing event JSON" + eventId + e.getMessage());
                            Toast.makeText(context, "Fehler", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Volley" + error.getMessage());
                Toast.makeText(context, "Fehler", Toast.LENGTH_LONG).show();
            }
        });

        stringRequest.setTag(tag);
        // Add the request to the RequestQueue.
        getRequestQueue(context).add(stringRequest);
    }


    /**
     * Load all Competitions from EventDetailFragment (onClick of a single Event)
     *
     * @param event
     * @param context
     * @param competitionsAdapter
     * @param tag
     * @param progressBar
     */
    public void updateCompetitionsAdapterOnline(DataContainer.Event event, final Context context, final CompetitionsAdapter competitionsAdapter, Object tag, final SwipeRefreshLayout progressBar) {
        User user = UserController.getInstance().getUser(context);

        //end here if no user id available
        if (user == null || user.id <= 0) {
            Log.e(TAG, "Can´t send request without userID");
            if (progressBar != null)
                progressBar.setRefreshing(false);
            return;
        }

        if(event == null) {
            Log.e(TAG, "Can´t send request without eventID");
            if (progressBar != null)
                progressBar.setRefreshing(false);
            return;
        }

        final List<Competition> competitions = new ArrayList<>();
        String url = config.api_company_events + event.id + "/" + user.id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //parse response json string
                        try {
                            //get all competitions from json string
                            JSONObject data = new JSONObject(response);
                            JSONArray jsonEvents = data.getJSONArray("data");

                            //convert response data to arrayList competitions
                            for (int i = 0; i < jsonEvents.length(); i++) {
                                //get event from json
                                JSONObject competition = jsonEvents.getJSONObject(i);

                                //convert competition to custom data structure competition
                                competitions.add(new Competition(
                                        competition.getInt("id"),
                                        competition.getString("com_name"),
                                        competition.getInt("com_category_id"),
                                        competition.getInt("com_status"),
                                        dateFormat.parse(competition.getString("com_start")),
                                        dateFormat.parse(competition.getString("com_end")),
                                        competition.getInt("com_size"),
                                        competition.getInt("com_sub_count"),
                                        competition.getString("com_descr"),
                                        competition.getInt("com_company_id"),
                                        competition.getBoolean("com_is_enlisted")
                                ));
                            }
                            //save offline
                            DataContainer.getInstance().saveCompetitionsData(competitions, context);

                            //update adapter
                            competitionsAdapter.updateData(competitions);

                            if (progressBar != null)
                                progressBar.setRefreshing(false);

                        } catch (Exception e) {
                            //
                            Log.e(TAG, "ERROR parsing events JSON");
                            progressBar.setRefreshing(false);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Volley" + error.getMessage());
                progressBar.setRefreshing(false);
            }
        });

        if (progressBar != null)
            progressBar.setRefreshing(true);

        stringRequest.setTag(tag);
        // Add the request to the RequestQueue.
        getRequestQueue(context).add(stringRequest);
    }

    public void saveCompetitionsData(List<Competition> competitionsData, Context ctx) {
        SharedPreferences _competitions = ctx.getSharedPreferences(ctx.getResources().getString(R.string.competition_data_prefs_key), ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = _competitions.edit();
        editor.putString(ctx.getResources().getString(R.string.competition_data_key), new Gson().toJson(competitionsData));
        editor.apply();

        Log.i(TAG, "save competitions data offline");
    }

    /**
     * Loca saved competition data
     * @param ctx
     * @return
     */
    public List<Competition> getCompetitionsData(Context ctx) {
        SharedPreferences _competitions = ctx.getSharedPreferences(ctx.getResources().getString(R.string.competition_data_prefs_key), ctx.MODE_PRIVATE);
        List<Competition> _competitionsList = new ArrayList<>();
        String dataString = _competitions.getString(ctx.getResources().getString(R.string.competition_data_key), "");
        try {
            JSONArray dataArray = new JSONArray(dataString);

            for(int i = 0; i < dataArray.length(); i++) {
                JSONObject comp = dataArray.getJSONObject(i);
                _competitionsList.add(new Gson().fromJson(comp.toString(), DataContainer.Competition.class));
            }
        } catch (Exception e) {
            Log.e(TAG, "Error parsing loaded competition data offline");
           // e.printStackTrace();
        }


        Log.i(TAG, "load competitions data offline");
        return _competitionsList;
    }

    public void updateMyEventsAdapterOnline(final Context context, final MyEventsAdapter myEventsAdapter, final SwipeRefreshLayout progressBar) { //updateMyCompetitions
        User user = UserController.getInstance().getUser(context);
        //end here if no user id available
        if (user == null || user.id <= 0) {
            Log.e(TAG, "Can´t send request without userID");
            return;
        }

        //setup volley
        myCompetitions = new ArrayList<>();
        String url = config.api_user_competitions + user.id;

        //setup params
        final Map<String, String> params = new HashMap<>();
        params.put("userID", "" + user.id);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //parse response json string
                        try {
                            //get all competitions from json string
                            JSONObject data = new JSONObject(response);
                            JSONArray jsonEvents = data.getJSONArray("data");

                            //convert response data to arrayList competitions
                            for (int i = 0; i < jsonEvents.length(); i++) {
                                //get event from json
                                JSONObject competition = jsonEvents.getJSONObject(i);

                                //convert competition to custom data structure competition
                                myCompetitions.add(new Competition(
                                        competition.getInt("id"),
                                        competition.getString("com_name"),
                                        competition.getInt("com_category_id"),
                                        competition.getInt("com_status"),
                                        (dateFormat.parse(competition.getString("com_start"))),
                                        dateFormat.parse(competition.getString("com_end")),
                                        competition.getInt("com_size"),
                                        competition.getInt("com_sub_count"),
                                        competition.getString("com_descr"),
                                        competition.getInt("com_company_id")
                                ));
                            }
                            myEventsAdapter.updateData(myCompetitions, context);

                            if (progressBar != null)
                                progressBar.setRefreshing(false);

                        } catch (Exception e) {
                            //
                            Log.e(TAG, "ERROR parsing events JSON");
                            progressBar.setRefreshing(false);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Volley");
                progressBar.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        if (progressBar != null)
            progressBar.setRefreshing(true);

        // Add the request to the RequestQueue.
        getRequestQueue(context).add(stringRequest);
    }


    /**
     * Try enlist user to course
     */
    public void EnlistUser(final Context ctx, final Competition competition, final CompetitionsAdapter competitionsAdapter,
                           final SwipeRefreshLayout progressBar, final View button, final int eventIdFK) {

        User user = UserController.getInstance().getUser(ctx);

        //end here if no user id available
        if (user == null || user.id <= 0) {
            Log.e(TAG, "Can´t send request without userID");
            return;
        }

        //end here if no user id available
        if (competition == null || competition.id <= 0) {
            Log.e(TAG, "Can´t send request without competitionID");
            return;
        }

        //setup volley
        String url = config.api_user_enlist + user.id + "/" + competition.id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //parse response json string
                        try {
                            //get all competitions from json string
                            JSONObject data = new JSONObject(response);
                            JSONObject error = data.getJSONObject("data");
                            int responseCode = error.getInt("error");

                            if (responseCode == 1) {
                                Log.i(TAG, "Error enlisting to competition: " + competition.id);
                                Toast.makeText(ctx, "Anmelden fehlgeschlagen. Bitte erneut versuchen", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.i(TAG, "user is enlisted to competition: " + competition.id);
                                //change btn
                                ((Button) button).setText(ctx.getResources().getString(R.string.btn_un_list_text));
                                ((Button) button).setBackgroundColor(Color.RED);
                            }

                            //notify change
                            updateCompetitionsAdapterOnline(new Event(eventIdFK, "", null), ctx, competitionsAdapter, null, progressBar);

                            if (progressBar != null)
                                progressBar.setRefreshing(false);

                        } catch (Exception e) {
                            //
                            Log.e(TAG, "ERROR parsing events JSON");
                            progressBar.setRefreshing(false);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Volley");
                progressBar.setRefreshing(false);
            }
        });

        if (progressBar != null)
            progressBar.setRefreshing(true);

        // Add the request to the RequestQueue.
        getRequestQueue(ctx).add(stringRequest);
    }

    /**
     * Try remove user from course
     */
    public void UnlistUser(final Context ctx, final Competition competition, final CompetitionsAdapter competitionsAdapter,
                           final SwipeRefreshLayout progressBar, final View button, final int eventIdFK) {

        User user = UserController.getInstance().getUser(ctx);

        //end here if no user id available
        if (user == null || user.id <= 0) {
            Log.e(TAG, "Can´t send request without userID");
            return;
        }

        //end here if no user id available
        if (competition == null || competition.id <= 0) {
            Log.e(TAG, "Can´t send request without competitionID");
            return;
        }

        //setup volley
        String url = config.api_user_unlist + user.id + "/" + competition.id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //parse response json string
                        try {
                            //get all competitions from json string
                            JSONObject data = new JSONObject(response);
                            JSONObject error = data.getJSONObject("data");
                            int responseCode = error.getInt("error");

                            if (responseCode == 1) {
                                Log.i(TAG, "Error unlisting to competition: " + competition.id);
                                Toast.makeText(ctx, "Abmelden fehlgeschlagen. Bitte erneut versuchen", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.i(TAG, "user is unlisted to competition: " + competition.id);
                                ((Button) button).setText(ctx.getResources().getString(R.string.btn_enlist_text));
                                ((Button) button).setBackgroundColor(Color.GREEN);
                            }

                            //notify change
                            updateCompetitionsAdapterOnline(new Event(eventIdFK, "", null), ctx, competitionsAdapter, null, progressBar);

                            if (progressBar != null)
                                progressBar.setRefreshing(false);

                        } catch (Exception e) {
                            //
                            Log.e(TAG, "ERROR parsing events JSON");
                            progressBar.setRefreshing(false);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Volley");
                progressBar.setRefreshing(false);
            }
        });

        if (progressBar != null)
            progressBar.setRefreshing(true);

        // Add the request to the RequestQueue.
        getRequestQueue(ctx).add(stringRequest);
    }


    public void cancel(Object tag) {
        if (requestQueue != null)
            requestQueue.cancelAll(tag);
        if (asyncFillEventsAdapter != null && tag instanceof EventsFragment) {
            asyncFillEventsAdapter.cancel(true);
            asyncFillEventsAdapter = null;
        }
    }

    /***
     * Persistency stuff
     *
     */


    /**
     * Save current state (UI user is one)
     * @param state
     * @param ctx
     */
    public void saveState(final String state, Context ctx) {
        SharedPreferences currentState = ctx.getSharedPreferences(ctx.getResources().getString(R.string.state_pref_key), ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = currentState.edit();
        editor.putString(ctx.getResources().getString(R.string.state_key), state);
        editor.commit();
    }

    /**
     * get current state - (UI user is one)
     * @param ctx
     * @return
     */
    public String getState(Context ctx) {
        SharedPreferences _state = ctx.getSharedPreferences(ctx.getResources().getString(R.string.state_pref_key), ctx.MODE_PRIVATE);
        String state = _state.getString(ctx.getResources().getString(R.string.state_key), "");
        Log.i(TAG, "load current state: " + state);
        return state;
    }

    /**
     * Save Events
     *
     * @param singleEvent
     * @param ctx
     */
    public void saveSingleEventData(DataContainer.Event singleEvent, Context ctx) {
        SharedPreferences eventsData = ctx.getSharedPreferences(ctx.getResources().getString(R.string.events_data_prefs_key), ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = eventsData.edit();

        String dataString = new Gson().toJson(singleEvent);
        editor.putString(ctx.getResources().getString(R.string.single_events_key), dataString);
        editor.apply();

        Log.i(TAG, "Saved single event data offline");
    }

    public DataContainer.Event getSingleEventData(Context ctx) {
        SharedPreferences singleEventData = ctx.getSharedPreferences(ctx.getResources().getString(R.string.events_data_prefs_key), ctx.MODE_PRIVATE);
        String dataString = singleEventData.getString(ctx.getResources().getString(R.string.single_events_key), "");
        Log.i(TAG, "Load single event data offline");
        DataContainer.Event _singleEvent = null;

        try {
            _singleEvent = new Gson().fromJson(dataString, DataContainer.Event.class);
        } catch (Exception e) {
            Log.e(TAG, "Error parsing single event data offline");
            //e.printStackTrace();
        }

        return _singleEvent;
    }


}
